from flask import render_template, Flask, request

from model_trainer_module.prediction.ProfileBuilder import ProfileBuilder
app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
profile_builder = ProfileBuilder()


@app.route('/', methods=["GET", "POST"])
def index():
    if request.method == "POST":
        form_fields_dict = request.form.to_dict()
        username = form_fields_dict['Username']
        age = form_fields_dict['age']
        gender = form_fields_dict['gender']
        emotion = form_fields_dict['emotion']
        sentiment = form_fields_dict['sentiment']
        personality = form_fields_dict['personality']
        ethnicity = form_fields_dict['location']  # Checks this one only for location
        location = form_fields_dict['location']
        topics = form_fields_dict['topics']

        prediction = profile_builder.predict(username, age, gender, emotion, sentiment, personality, ethnicity,
                                             location, topics)
        if prediction is None:
            return render_template("index.html", error=True)

        return render_template("index.html",
                               username=username,
                               gender=prediction['gender'],
                               gender_explain1=list_to_dict(prediction['gender_explain1']),
                               gender_explain2=list_to_dict(prediction['gender_explain2']),

                               age=prediction['age'],
                               age_groups=prediction['age_groups'],
                               age_explain=list_to_dict(prediction['age_explain']),

                               sentiment=trim_float(prediction['sentiment']),
                               sentiment_explain_positive=prediction['sentiment_explain_positive'],
                               sentiment_explain_neutral=prediction['sentiment_explain_neutral'],
                               sentiment_explain_negative=prediction['sentiment_explain_negative'],
                               # sentiment_explain_positive=dict_to_dict(prediction['sentiment_explain']['positive']),
                               # sentiment_explain_neutral=dict_to_dict(prediction['sentiment_explain']['neutral']),
                               # sentiment_explain_negative=dict_to_dict(prediction['sentiment_explain']['negative']),

                               # Emotion
                               anger=prediction['emotion']['anger'],
                               disgust=prediction['emotion']['disgust'],
                               fear=prediction['emotion']['fear'],
                               joy=prediction['emotion']['joy'],
                               sadness=prediction['emotion']['sadness'],
                               surprise=prediction['emotion']['surprise'],
                               anger_explain=prediction['all_emotion_explanations']['anger'],
                               disgust_explain=prediction['all_emotion_explanations']['disgust'],
                               fear_explain=prediction['all_emotion_explanations']['fear'],
                               joy_explain=prediction['all_emotion_explanations']['joy'],
                               sadness_explain=prediction['all_emotion_explanations']['sadness'],
                               surprise_explain=prediction['all_emotion_explanations']['surprise'],

                               # Personality
                               openness=prediction['openness'][0],
                               openness_explain=list_to_dict(prediction['openness'][1]),
                               conscientious=prediction['conscientious'][0],
                               conscientious_explain=list_to_dict(prediction['conscientious'][1]),
                               extraversion=prediction['extraversion'][0],
                               extraversion_explain=list_to_dict(prediction['extraversion'][1]),
                               agreeableness=prediction['agreeableness'][0],
                               agreeableness_explain=list_to_dict(prediction['agreeableness'][1]),
                               neuroticism=prediction['neuroticism'][0],
                               neuroticism_explain=list_to_dict(prediction['neuroticism'][1]),
                               o=prediction['O'],
                               c=prediction['C'],
                               e=prediction['E'],
                               a=prediction['A'],
                               n=prediction['N'],

                               tagcloud=prediction['tagcloud'],
                               wordcloud=prediction['wordcloud'],
                               lda=prediction['lda'],
                               bigrams=prediction['bigrams'],
                               ethnicity=prediction['ethnicity'],
                               location=prediction['location'],
                               error=False)
    else:
        return render_template("index.html", error=False)


def list_to_dict(list_: list):
    list_items = {}

    if not list_:
        return None

    for item in list_:
        list_items[item[0]] = item[1]
    return list_items


def dict_to_dict(list_: dict):
    list_items = {}

    if not list_:
        return None

    for key, value in list_.items():
        list_items[key] = value
    return list_items


def trim_float(numbers: dict):

    if not numbers:
        return None

    converted_numbers = {}
    for key, value in numbers.items():
        converted_numbers[key] = round(value, 4)
    return converted_numbers


def run(debug=True, port=8080):
    app.run(debug=debug, port=port)


if __name__ == '__main__':
    app.run(debug=True)
