import os
import pickle

import pandas as pd
from lime.lime_text import LimeTextExplainer
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline

from model_trainer_module.feature_extraction.feature_extractors.EmotionTextFeatureExtractor import EmotionTextFeatureExtractor

from model_trainer_module.utils.path_utils import resources_path
from model_trainer_module.prediction.utils.models_performance import performance


def get_model():
    rf = RandomForestClassifier(bootstrap=False, max_features='sqrt', n_estimators=400)
    svm = SVC()
    lr = LogisticRegression()
    nb = MultinomialNB()
    return Pipeline([
        ('vect', CountVectorizer(strip_accents='unicode',
                                 analyzer="word",
                                 token_pattern=r"[\w\-:]+",
                                 stop_words="english",
                                 max_df=0.85,
                                 ngram_range=(1, 2),
                                 lowercase=True)),
        ('clf', rf)
    ])


def dummy_func(doc):
    return doc


class EmotionClassifier:
    """
    This class manages the loading or creation of the supervised emotion ML model
    """
    feature_extractor = None

    vectorizer_filename = resources_path("models/emotion/emotion_count_vec.sav")
    vectorizer = None

    anger_filename = resources_path("models/emotion/emotion_anger_model.sav")
    disgust_filename = resources_path("models/emotion/emotion_disgust_model.sav")
    fear_filename = resources_path("models/emotion/emotion_fear_model.sav")
    joy_filename = resources_path("models/emotion/emotion_joy_model.sav")
    sadness_filename = resources_path("models/emotion/emotion_sadness_model.sav")
    surprise_filename = resources_path("models/emotion/emotion_surprise_model.sav")
    # model = None
    anger = get_model()
    disgust = get_model()
    fear = get_model()
    joy = get_model()
    sadness = get_model()
    surprise = get_model()

    def __init__(self):
        self.feature_extractor = EmotionTextFeatureExtractor()
        # Checks only one model ~~~~~~~~~~
        if os.path.exists(self.anger_filename):  # os.path.exists(self.vectorizer_filename) and
            try:
                # self.vectorizer = pickle.load(open(self.vectorizer_filename, "rb"))
                self.anger = pickle.load(open(self.anger_filename, "rb"))
                self.disgust = pickle.load(open(self.disgust_filename, "rb"))
                self.fear = pickle.load(open(self.fear_filename, "rb"))
                self.joy = pickle.load(open(self.joy_filename, "rb"))
                self.sadness = pickle.load(open(self.sadness_filename, "rb"))
                self.surprise = pickle.load(open(self.surprise_filename, "rb"))
                print("Loaded persisted emotion model.")
                return
            except Exception:
                print("Failed to load model. Resuming model creation...")

        filepath = resources_path("dataset/emotion/2018-E-c-En-train.txt")
        raw_data = pd.read_csv(filepath, delimiter="\t")
        data = self.feature_extractor.extract_transform_dataset(raw_data)

        data.drop([0, 3, 7, 8, 9, 12], axis=1, inplace=True)
        data.columns = ['Tweet', 'anger', 'disgust', 'fear', 'joy', 'sadness', 'surprise']

        X = data.iloc[:, 0]
        y = data.iloc[:, 1:]

        # split dataset to train and test
        x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=2)
        print("|-- Training emotion analysis models...")

        self.anger.fit(x_train, y_train['anger'])
        self.disgust.fit(x_train, y_train['disgust'])
        self.fear.fit(x_train, y_train['fear'])
        self.joy.fit(x_train, y_train['joy'])
        self.sadness.fit(x_train, y_train['sadness'])
        self.surprise.fit(x_train, y_train['surprise'])

        print("\n\nPrediction anger test metrics:")
        y_pred = self.anger.predict(x_test)
        performance(y_test['anger'], y_pred)

        print("\n\nPrediction disgust test metrics:")
        performance(y_test['disgust'], self.disgust.predict(x_test))

        print("\n\nPrediction fear test metrics:")
        performance(y_test['fear'], self.fear.predict(x_test))

        print("\n\nPrediction joy test metrics:")
        performance(y_test['joy'], self.joy.predict(x_test))

        print("\n\nPrediction sadness test metrics:")
        performance(y_test['sadness'], self.sadness.predict(x_test))

        print("\n\nPrediction surprise test metrics:")
        performance(y_test['surprise'], self.surprise.predict(x_test))

        print("|-- Emotion Models saving")
        pickle.dump(self.anger, open(self.anger_filename, "wb"))
        pickle.dump(self.disgust, open(self.disgust_filename, "wb"))
        pickle.dump(self.fear, open(self.fear_filename, "wb"))
        pickle.dump(self.joy, open(self.joy_filename, "wb"))
        pickle.dump(self.sadness, open(self.sadness_filename, "wb"))
        pickle.dump(self.surprise, open(self.surprise_filename, "wb"))

    def emotions_of(self, words: list):
        """
        Uses the ML model to predict the emotions of the tokens and converts them to an Emotion object
        :param words: a list of preprocessed tokens
        :return: Emotion instance, explainability: array([probabilities], [(word, value), (word, value)])
        """
        # x = self.vectorizer.transform([words])
        x = words
        try:
            # anger = self.anger.predict(x)[0]
            # anger = self.anger.predict_proba(words)
            anger_explain = self.text_interpret(self.anger, ['anger'], words)
            # disgust = self.disgust.predict(x)[0]
            # disgust = self.disgust.predict_proba(x)[0][1]
            disgust_explain = self.text_interpret(self.disgust, ['disgust'], words)
            # fear = self.fear.predict(x)[0]
            # fear = self.fear.predict_proba(x)[0][1]
            fear_explain = self.text_interpret(self.fear, ['fear'], words)
            # joy = self.joy.predict(x)[0]
            # joy = self.joy.predict_proba(x)[0][1]
            joy_explain = self.text_interpret(self.joy, ['joy'], words)
            # sadness = self.sadness.predict(x)[0]
            # sadness = self.sadness.predict_proba(x)[0][1]
            sadness_explain = self.text_interpret(self.sadness, ['sadness'], words)
            # surprise = self.surprise.predict(x)[0]
            # surprise = self.surprise.predict_proba(x)[0][1]
            surprise_explain = self.text_interpret(self.surprise, ['surprise'], words)

            return anger_explain, disgust_explain, fear_explain, joy_explain, sadness_explain, surprise_explain
        except:
            return None, None, None, None, None, None

    def text_interpret(self, model, class_name, instance):
        """
        This method uses LIME to interpret the models based on text.

        :param model:
        :param class_name:
        :param instance: String to interpret
        :return: probabilities for each class(0,1), list of words that extracted by LIME
        """
        # instance = " ".join(instance)

        explainer = LimeTextExplainer()
        explanation = explainer.explain_instance(instance, model.predict_proba, num_features=10)
        feature_list = explanation.as_list()
        probabilities = explanation.predict_proba
        # explanation.as_pyplot_figure(label=1).show()
        print("| Interpret of {} model finished -\n\n".format(class_name))
        return probabilities, feature_list
