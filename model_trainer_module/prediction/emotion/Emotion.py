
from model_trainer_module.prediction.emotion.EmotionClassifier import EmotionClassifier


class Emotion:
    """
    This class handles the loading of relevant ML models prediction of emotions for a text.
    """
    features_extractor = None
    unsupervised_classifier = None
    supervised_classifier = None

    def __init__(self):
        self.supervised_classifier = EmotionClassifier()
        self.features_extractor = self.supervised_classifier.feature_extractor

    def detect_emotions_list(self, data: list):
        """
        Method that uses a hybrid approach
        :param data:
        :return: Emotion object, dictionary: explains for all basic emotion predictions
        """
        total_emotion = {
            'anger': 0,
            'disgust': 0,
            'fear': 0,
            'joy': 0,
            'sadness': 0,
            'surprise': 0,
        }
        emotions_explanations = {
            'anger': {},
            'disgust': {},
            'fear': {},
            'joy': {},
            'sadness': {},
            'surprise': {},
        }
        total = 0
        for text in data:
            words = self.features_extractor.extract_from_string(text)

            anger_explain, disgust_explain, fear_explain, joy_explain, sadness_explain, surprise_explain = self.supervised_classifier.emotions_of(
                words)
            if not anger_explain or not words:
                continue
            total = total + 1

            emotions_explanations = self.explanations_concat(emotions_explanations, anger_explain[1],
                                                             disgust_explain[1], fear_explain[1], joy_explain[1],
                                                             sadness_explain[1], surprise_explain[1])
            total_emotion['anger'] += anger_explain[0][1]
            total_emotion['disgust'] += disgust_explain[0][1]
            total_emotion['fear'] += fear_explain[0][1]
            total_emotion['joy'] += joy_explain[0][1]
            total_emotion['sadness'] += sadness_explain[0][1]
            total_emotion['surprise'] += surprise_explain[0][1]

        total = total if total != 0 else 1
        a = {k: round((v / total) * 100, 0) for k, v in total_emotion.items()}
        return a, emotions_explanations

    def explanations_concat(self, summarized_explanations, anger_explain, disgust_explain, fear_explain, joy_explain,
                            sadness_explain, surprise_explain):

        summarized_explanations['anger'] = self.dictionary_handle(summarized_explanations['anger'], anger_explain,
                                                                  'anger_explain')
        summarized_explanations['disgust'] = self.dictionary_handle(summarized_explanations['disgust'], disgust_explain,
                                                                    'disgust_explain')
        summarized_explanations['fear'] = self.dictionary_handle(summarized_explanations['fear'], fear_explain,
                                                                 'fear_explain')
        summarized_explanations['joy'] = self.dictionary_handle(summarized_explanations['joy'], joy_explain,
                                                                'joy_explain')
        summarized_explanations['sadness'] = self.dictionary_handle(summarized_explanations['sadness'], sadness_explain,
                                                                    'sadness_explain')
        summarized_explanations['surprise'] = self.dictionary_handle(summarized_explanations['surprise'],
                                                                     surprise_explain,
                                                                     'surprise_explain')

        return summarized_explanations

    def detect_emotions(self, text):
        """

        :param text: a string of the text to predict emotions for
        :return: Emotion instance, supervised explainability(dictionary)
        """
        words = self.features_extractor.extract_from_string(text)

        supervised_sentiment, anger_explain, disgust_explain, fear_explain, joy_explain, sadness_explain, surprise_explain = self.supervised_classifier.emotions_of(
            words)
        unsupervised_sentiment = self.unsupervised_classifier.emotions_of(words)

        emotion = Emotion()
        emotion.values_by_averaging([supervised_sentiment, unsupervised_sentiment])
        emotion.min_max_normalize()

        return emotion, anger_explain, disgust_explain, fear_explain, joy_explain, sadness_explain, surprise_explain

    def detect_sentiment_supervised(self, text):
        """
        Detect the emotions of the text using the supervised approach
        :param text: a string of the text to predict emotions for
        :return: Emotion instance
        """
        words = self.features_extractor.transform_string(text)

        supervised_emotions, anger_explain, disgust_explain, fear_explain, joy_explain, sadness_explain, surprise_explain = self.supervised_classifier.emotions_of(
            words)
        supervised_emotions.min_max_normalize()
        explains = {
            "anger_explain": anger_explain,
            "disgust_explain": disgust_explain,
            "fear_explain": fear_explain,
            "joy_explain": joy_explain,
            "sadness_explain": sadness_explain,
            "surprise_explain": surprise_explain
        }
        return supervised_emotions, explains

    def dictionary_handle(self, existing_explanations, new_dict_to_add, emotion_class):
        keys = existing_explanations.keys()
        for word in new_dict_to_add:
            if word[0] in keys:
                existing_explanations[word[0]] += word[1]
            else:
                existing_explanations[word[0]] = word[1]
        return existing_explanations
