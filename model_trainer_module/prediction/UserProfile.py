# gender constants
GENDER_MALE = 0
GENDER_FEMALE = 1
GENDER_OTHER = 2

# age constants
AGE_18_24 = "18-24"
AGE_25_34 = "25-34"
AGE_35_49 = "35-49"
AGE_50_XX = "50-XX"

