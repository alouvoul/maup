from sklearn.model_selection import train_test_split

from model_trainer_module.prediction.sentiment.SentimentClassifier import SentimentClassifier
from model_trainer_module.prediction.utils.models_performance import performance
from model_trainer_module.preprocess.preprocess import preprocess
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import pandas as pd

from model_trainer_module.utils.path_utils import resources_path
vader_predict = False


class Sentiment:
    classifier = None

    def __init__(self):
        print("|---- Initialize sentiment classifier")
        if not vader_predict:
            self.classifier = SentimentClassifier()
        else:
            self.classifier = SentimentIntensityAnalyzer()
        print("|---- Initialize sentiment classifier ended")

    def sentiment_of(self, text: str):
        return self.classifier.sentiment_of(text)

    def sentiment_of_tweets(self, tweets: list):
        total_sentiment = {
            'positive': 0,
            'negative': 0,
            'neutral': 0,
        }

        tweets_per_sent = {
            'positive': [],
            'negative': [],
            'neutral': [],
        }
        list_size = 0
        for tweet in tweets:
            t = preprocess(tweet)
            if not t:
                continue
            list_size = list_size + 1

            if vader_predict:
                vader_result = self.test_vader(tweet)
                if vader_result is 'pos':
                    prediction = 'positive'
                elif vader_result is 'neg':
                    prediction = 'negative'
                else:
                    prediction = 'neutral'
                total_sentiment[prediction] += 1
                tweets_per_sent[prediction].append(tweet)
            else:
                predictions = self.classifier.sentiment_of(t)
                total_sentiment[predictions[0]] += 1
                tweets_per_sent[predictions[0]].append(tweet)
        list_size = list_size if list_size > 0 else 1
        total_sentiment['positive'] = total_sentiment['positive'] / list_size
        total_sentiment['negative'] = total_sentiment['negative'] / list_size
        total_sentiment['neutral'] = total_sentiment['neutral'] / list_size
        return total_sentiment, tweets_per_sent

    def test_vader(self, tweet):
        """
        Method to get the prediction from Vader sentiment. This method used alternative to the trained model of the Mediator
        platform
        :param tweet: String
        :return: string of neg/pos/neu from the Vader prediction
        """
        vader_result = self.classifier.polarity_scores(tweet)
        compound = vader_result.pop('compound')
        if compound <= -0.05:
            vader_results = 'neg'
        elif compound >= 0.05:
            vader_results = 'pos'
        else:
            vader_results = 'neu'
        return vader_results

    def summarize_feature_importance(self, feature_importance: dict, instance_feature_importance: dict):
        """
        Method to extract important words for each sentiment class based on Interpretability results.

        :param feature_importance:
        :param instance_feature_importance:
        :return:
        """
        for feature in instance_feature_importance['positive']:
            if feature[1] > 0:
                feature_importance['positive'][feature[0]] = feature[1]

        for feature in instance_feature_importance['neutral']:
            if feature[1] > 0:
                feature_importance['neutral'][feature[0]] = feature[1]

        for feature in instance_feature_importance['negative']:
            if feature[1] > 0:
                feature_importance['negative'][feature[0]] = feature[1]

        return feature_importance
