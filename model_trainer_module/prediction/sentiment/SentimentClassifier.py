import pickle

import nltk
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB

import pandas as pd

from imblearn.pipeline import Pipeline
from lime.lime_text import LimeTextExplainer
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.pipeline import FeatureUnion
from sklearn.svm import SVC

from model_trainer_module.feature_extraction.feature_extractors.SentimentFeatureExtractor import \
    SentimentFeatureExtractor
from model_trainer_module.preprocess.ModelPreprocessor import ModelPreprocessor
from model_trainer_module.prediction.utils.model_reader import model_reader
from model_trainer_module.prediction.utils.models_performance import performance
from model_trainer_module.utils.path_utils import resources_path


class SentimentClassifier:
    model = None

    model_filename = resources_path("models/sentiment/sentiment_classifier.sav")
    dataset_filename = resources_path("dataset/sentiment/sentiment-semval.txt")
    interpreter = None

    def __init__(self):
        self.model = model_reader(self.model_filename)
        if self.model is not None:
            return

        data = pd.read_csv(self.dataset_filename, names=['id', 'sentiment', 'tweet'], delimiter="\t")
        print("-------- Sentiment dataset Statistics -----------\n")
        print(data.drop_duplicates().groupby('sentiment').count())
        print("------------------------------------------------\n")

        x_train, x_test, y_train, y_test = train_test_split(data['tweet'], data['sentiment'], test_size=0.3)

        mp = ModelPreprocessor()
        train = mp.transform(x_train)
        self.__train_model(train, y_train)

        test = mp.transform(x_test)
        y_pred = self.model.predict(test)
        performance(y_pred, y_test)

        print("Persisting Models...")
        pickle.dump(self.model, open(self.model_filename, "wb"))

    def __train_model(self, x_train, y_train):
        rf = RandomForestClassifier(n_estimators=1600, min_samples_split=10, min_samples_leaf=2, max_features='auto', max_depth=None, bootstrap=True)
        # sf = SentimentFeatureExtractor()
        svm = SVC(kernel='linear', gamma=1, C=1, probability=True)
        nb = MultinomialNB(fit_prior=True, alpha=1)
        lr = LogisticRegression(solver='newton-cg', penalty='l2', C=10)
        estimators = [("svm", svm), ("rf", rf), ("nb", nb), ("lr", lr)]
        self.model = Pipeline(steps=[
            ('u1', FeatureUnion([
                ('vectorizer', Pipeline([
                    ('cv', CountVectorizer(strip_accents='unicode',
                                           analyzer="word",
                                           token_pattern=r"[\w\-:]+",
                                           preprocessor=dummy_func,
                                           tokenizer=dummy_func,
                                           min_df=7,
                                           max_df=0.85,
                                           ngram_range=(1, 2),
                                           lowercase=False)),
                ])),
                # ('pos', Pipeline([
                #     ('text', PosTagger()),
                #     ('cv', CountVectorizer(strip_accents='unicode',
                #                            analyzer="word",
                #                            token_pattern=r"[\w\-:]+",
                #                            preprocessor=dummy_func,
                #                            tokenizer=dummy_func,
                #                            ngram_range=(1, 2),
                #                            lowercase=False)),
                # ])),
                ('text', Pipeline([
                    ('sentiment_features', SentimentFeatureExtractor()),
                ])),
            ])),
            ('clf', VotingClassifier(estimators, voting='soft', n_jobs=1))])
        self.model.fit(x_train, y_train)

    def interpret(self, instance):
        """
        Interpret a classifier that uses text BoW as features
        :param instance:
        :return:
        """

        class_names = ['positive', 'negative', 'neutral']
        explainer = LimeTextExplainer(class_names=class_names)
        explanation = explainer.explain_instance(instance, self.model.predict_proba, num_features=10, top_labels=3)
        feature_list1 = explanation.as_list(label=0)
        feature_list2 = explanation.as_list(label=1)
        feature_list3 = explanation.as_list(label=2)

        print("| Interpret of sentiment first model finished -\n\n")
        return {
            'positive': feature_list1,
            'negative': feature_list2,
            'neutral': feature_list3
        }

    def sentiment_of(self, text):
        """

        :param text: Tweet to predict the sentiment
        :return: Sentiment of the tweet based on the model prediction
        """
        return self.model.predict([text])


def dummy_func(doc):
    return doc
