import pandas as pd
import datetime

from model_trainer_module.prediction.demographics.age.Age import Age
from model_trainer_module.prediction.demographics.ethnicity.EthnicityDetector import EthnicityDetector
from model_trainer_module.prediction.demographics.gender.Gender import Gender
from model_trainer_module.prediction.emotion.Emotion import Emotion
from model_trainer_module.prediction.sentiment.Sentiment import Sentiment
from model_trainer_module.prediction.topics.Topics import Topics
from model_trainer_module.scrap.TwitterAPI import TwitterAPI
from model_trainer_module.prediction.personality.Personality import Personality
from model_trainer_module.prediction.demographics.location.location import GeoLocation

# TODO na to valw 100
tweet_count = 20
TOTAL_NUMBER_OF_TWEETS_PER_SENTIMENT = 2


class ProfileBuilder:
    """
    This class initialize each model for the platform. Also used to select each of the user profiling aspects have been
    selected by the user and re turns the results.
    """
    age_classifier = None
    gender_classifier = None
    emotion_classifier = None
    sentiment_classifier = None
    personality_classifier = None
    location_finder = None
    ethnicity = None
    topics = None

    def __init__(self):
        self.tw = TwitterAPI()
        self.gender_classifier = Gender()
        self.age_classifier = Age()
        self.sentiment_classifier = Sentiment()
        self.emotion_classifier = Emotion()
        self.personality_classifier = Personality()
        self.location_finder = GeoLocation()
        self.ethnicity = EthnicityDetector()
        self.topics = Topics()

    def predict(self, screen_name, age, gender, emotion, sentiment, personality, ethnicity, location, topics):
        user = self.tw.get_user(screen_name)
        tweets = self.tw.get_user_tweets_list(screen_name, tweet_count)

        if not user or not tweets:
            return None

        aspects = {}

        if gender == '1':
            gender, gender_explain1, gender_explain2 = self.gender(user, tweets)
            aspects['gender'] = gender
            aspects['gender_explain1'] = gender_explain1
            aspects['gender_explain2'] = gender_explain2
        else:
            aspects['gender'] = None
            aspects['gender_explain1'] = []
            aspects['gender_explain2'] = []

        if age == '1':
            age, age_explain = self.age(tweets)
            aspects['age'] = 1
            aspects['age_groups'] = age
            aspects['age_explain'] = age_explain

            if zero_importance_features(aspects['age_explain']) is None:
                aspects['age_explain'] = None
        else:
            aspects['age'] = None
            aspects['age_groups'] = None
            aspects['age_explain'] = []

        if sentiment == '1':
            aspects['sentiment'], aspects['sentiment_explain'] = self.sentiment(tweets)
            aspects['sentiment_explain_positive'] = aspects['sentiment_explain']['positive'][
                                                    0:TOTAL_NUMBER_OF_TWEETS_PER_SENTIMENT]
            aspects['sentiment_explain_negative'] = aspects['sentiment_explain']['negative'][
                                                    0:TOTAL_NUMBER_OF_TWEETS_PER_SENTIMENT]
            aspects['sentiment_explain_neutral'] = aspects['sentiment_explain']['neutral'][
                                                   0:TOTAL_NUMBER_OF_TWEETS_PER_SENTIMENT]
        else:
            aspects['sentiment'], aspects['sentiment_explain'] = None, None
            aspects['sentiment_explain_positive'] = None
            aspects['sentiment_explain_negative'] = None
            aspects['sentiment_explain_neutral'] = None

        if emotion == '1':
            aspects['emotion'], aspects['all_emotion_explanations'] = self.emotion(tweets)
        else:
            aspects['emotion'] = {
                'anger': None,
                'disgust': None,
                'fear': None,
                'joy': None,
                'sadness': None,
                'surprise': None,
            }
            aspects['all_emotion_explanations'] = {
                'anger': None,
                'disgust': None,
                'fear': None,
                'joy': None,
                'sadness': None,
                'surprise': None,
            }

        if personality == '1':
            aspects['openness'], aspects['conscientious'], aspects['extraversion'], aspects['agreeableness'], aspects[
                'neuroticism'], lang_results = self.personality(user, tweets)
            aspects['O'] = lang_results['O'][0]
            aspects['C'] = lang_results['C'][0]
            aspects['E'] = lang_results['E'][0]
            aspects['A'] = lang_results['A'][0]
            aspects['N'] = lang_results['N'][0]
        else:
            aspects['openness'] = [None, None]
            aspects['conscientious'] = [None, None]
            aspects['extraversion'] = [None, None]
            aspects['agreeableness'] = [None, None]
            aspects['neuroticism'] = [None, None]
            aspects['O'] = None
            aspects['C'] = None
            aspects['E'] = None
            aspects['A'] = None
            aspects['N'] = None

        if ethnicity == '1':
            aspects['ethnicity'] = self.ethnicity_detection(tweets)
        else:
            aspects['ethnicity'] = None

        if location == '1':
            aspects['location'] = self.location(user.location)
        else:
            aspects['location'] = None

        if topics == '1':
            # tweets = self.tw.get_user_tweets_list(screen_name, tweet_count=50)
            aspects['tagcloud'], aspects['wordcloud'], aspects['lda'], aspects['bigrams'] = self.topics_extraction(
                tweets)
        else:
            aspects['tagcloud'], aspects['wordcloud'], aspects['lda'], aspects['bigrams'] = None, None, None, None,

        return aspects

    def age(self, tweets):
        return self.age_classifier.detect_age_list(tweets)

    def gender(self, user, tweets):
        user = user._json

        before = datetime.datetime.strptime(user['created_at'], '%a %b %d %H:%M:%S +%f %Y')
        now = datetime.datetime.now()
        years_active = float((now - before).days) / 365
        user_dict = {
            "years_active": years_active,
            "followers_count": user['followers_count'],
            "friends_count": user['friends_count'],
            "statuses_count": user['statuses_count'],
            "favourites_count": user['favourites_count'],
            "verified": user['verified'],
            "average_likes": int(user['statuses_count']) / years_active,
            "description": user['description'],
        }
        user = pd.DataFrame([user_dict])
        return self.gender_classifier.gender_of(user, tweets)

    def emotion(self, tweets):
        return self.emotion_classifier.detect_emotions_list(tweets)

    def sentiment(self, tweets):
        return self.sentiment_classifier.sentiment_of_tweets(tweets)

    def personality(self, user, tweets):
        # User
        user = user._json
        user_dict = {
            "twitter_id": user['id'],
            "name": user['name'],
            "screen_name": user['screen_name'],
            "followers_count": user['followers_count'],
            "friends_count": user['friends_count'],
            "listed_count": user['listed_count'],
            "statuses_count": user['statuses_count'],
            "favourites_count": user['favourites_count'],
            "description": user['description'],
            "created_at": user['created_at'],
            "location": user['location']
        }

        tweets_final = ''
        for tweet in tweets:
            tweets_final = tweets_final + ' ' + tweet

        user = pd.DataFrame([user_dict])
        data = user
        data['tweets'] = tweets_final
        return self.personality_classifier.personality_of(data)

    def location(self, tweets):
        return self.location_finder.geocode(tweets)

    def ethnicity_detection(self, tweets):
        total_text = ""
        for tweet in tweets:
            total_text = total_text + " " + tweet
        return self.ethnicity.ethnicity_detect(total_text)

    def topics_extraction(self, tweets):
        return self.topics.topics_of(tweets)


def zero_importance_features(features: list):
    temp = []
    for v in features:
        temp.append(v[1])
    temp.sort()
    if temp[0] == 0:
        return None
    else:
        return True

