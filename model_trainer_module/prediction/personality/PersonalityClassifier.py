import pickle
import pandas as pd

from imblearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error

from model_trainer_module.feature_extraction.feature_extractors.PersonalityFeatureExtractor import \
    PersonalityFeatureExtractor
from model_trainer_module.prediction.personality.PersonalityInterpretator import PersonalityInterpretator
from model_trainer_module.prediction.personality.models.AgreeablenessClassifier import AgreeablenessClassifier
from model_trainer_module.prediction.personality.models.ConscientiousnessClassifier import ConscientiousClassifier
from model_trainer_module.prediction.personality.models.ExtraversionClassifier import ExtraversionClassifier
from model_trainer_module.prediction.personality.models.NeuroticismClassifier import NeuroticismClassifier
from model_trainer_module.prediction.personality.models.OpennessClassifier import OpennessClassifier
from model_trainer_module.prediction.personality.models.personality_modeling.personality_features import \
    get_lang_based_scores

from model_trainer_module.preprocess.ModelPreprocessor import ModelPreprocessor
from model_trainer_module.prediction.utils.model_reader import model_reader

from model_trainer_module.utils.path_utils import resources_path


class PersonalityClassifier:
    openness_model = None
    conscientiousness_model = None
    extraversion_model = None
    agreeableness_model = None
    neuroticism_model = None

    lda = None
    model_filename = resources_path("models/personality/personality_classifier.sav")
    feature_transform_filename = resources_path("models/personality/personality_feature_transform.sav")
    dataset_filename = resources_path("dataset/personality/final_personality.csv")
    pfe = PersonalityFeatureExtractor()

    def __init__(self):
        data = pd.read_csv(self.dataset_filename, index_col='id')

        # FEATURES EXTRACTION SIZE
        data['screen_name'] = data['screen_name'].map(lambda x: len(x))
        data['description'] = data['description'].map(lambda x: self.size_of_text(x))

        # Train set
        x = data[[
            'twitter_uid', 'screen_name', 'description', 'favourites_count', 'followers_count', 'friends_count',
            'statuses_count', 'listed_count', 'tweets'
        ]]
        # Test set
        y = data[['OPEN', 'CONS', 'EXTR', 'AGRE', 'NEUR']]

        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)

        text_features = self.pfe.transform(docs=x_train['tweets'])
        x_train.drop(['tweets', 'twitter_uid'], axis=1, inplace=True)
        x_train = pd.concat(
            [text_features.reset_index(drop=True),
             x_train.reset_index(drop=True)],
            axis=1)

        text_features = self.pfe.transform(docs=x_test['tweets'])
        x_test.drop(['tweets', 'twitter_uid'], axis=1, inplace=True)
        x_test = pd.concat(
            [text_features.reset_index(drop=True),
             x_test.reset_index(drop=True),
             ], axis=1)

        # TRAIN models
        self.openness_model = OpennessClassifier(x_train, y_train['OPEN'].div(5), x_test, y_test['OPEN'].div(5))
        self.conscientiousness_model = ConscientiousClassifier(x_train, y_train['CONS'].div(5), x_test, y_test['CONS'].div(5))
        self.extraversion_model = ExtraversionClassifier(x_train, y_train['EXTR'].div(5), x_test, y_test['EXTR'].div(5))
        self.agreeableness_model = AgreeablenessClassifier(x_train, y_train['AGRE'].div(5), x_test, y_test['AGRE'].div(5))
        self.neuroticism_model = NeuroticismClassifier(x_train, y_train['NEUR'].div(5), x_test, y_test['NEUR'].div(5))

    def __train_transformer(self, x_train):
        self.feature_transformer = model_reader(self.feature_transform_filename)
        if self.feature_transformer:
            return
        pfe = PersonalityFeatureExtractor()

        mp = ModelPreprocessor()
        x_train = mp.transform(x_train)

        self.feature_transformer = Pipeline([
            ('cv', TfidfVectorizer(strip_accents='unicode',
                                   analyzer="word",
                                   token_pattern=r"[\w\-:]+",
                                   preprocessor=dummy_func,
                                   tokenizer=dummy_func,
                                   min_df=10,
                                   max_df=0.85,
                                   ngram_range=(1, 2),
                                   lowercase=False)
             ),
        ]).fit(x_train)
        print("Persisting transformer...")
        pickle.dump(self.feature_transformer, open(self.feature_transform_filename, "wb"))

    def personality_of(self, data):
        """

        :param data:
        :return: Explainability results, Personality scores of the user based on the model prediction
        """

        data['screen_name'] = data['screen_name'].map(lambda x: len(x))
        data['description'] = data['description'].map(lambda x: self.size_of_text(x))

        y_text_features = self.pfe.transform(docs=data['tweets'])
        # Get OCEAN predictions from language
        lang_results = self.personality_of_user_lang_based(tweets=data)

        data.drop(['tweets', 'twitter_id', 'name', 'created_at', 'location'], axis=1, inplace=True)
        data = pd.concat(
            [data.reset_index(drop=True),
             y_text_features.reset_index(drop=True)], axis=1)

        # data_to_predict = pd.concat([data.drop(['tweets'], inplace=True), transformed_features], axis=1)
        data = data[['total_number_mentions', 'total_number_hashtags', 'words_average_length', 'capital_letters', 'post_length', 'is_retweet', 'flooding', 'emojies', 'period_count', 'multi_period_count', 'negation', 'sentiment', 'no_of_links', 'screen_name', 'description', 'favourites_count', 'followers_count', 'friends_count', 'statuses_count', 'listed_count']]
        openness_results = self.openness_model.predict_explain(data)
        conscientious_results = self.conscientiousness_model.predict_explain(data)
        extraversion_results = self.extraversion_model.predict_explain(data)
        agreeableness_results = self.agreeableness_model.predict_explain(data)
        neuroticism_results = self.neuroticism_model.predict_explain(data)

        return openness_results, conscientious_results, extraversion_results, agreeableness_results, neuroticism_results, lang_results

    def personality_of_user_lang_based(self, tweets: pd.DataFrame):
        """
        This method uses a pre-trained regressor that uses text to predict the classes. Utilizing text predictions may be
        more accurate than the tabular data that used for the other model.
        :param tweets: Dataframe which contains strings
        :return: List of the predictions
        """
        data = tweets.rename(columns={'twitter_id': 'user_id', 'tweets': 'text'})
        users_with_personality = get_lang_based_scores(data)
        print(users_with_personality)
        return users_with_personality

    def size_of_text(self, text):
        if pd.isna(text):
            return 0
        text_size = len([token.lower() for token in text.split(" ")])
        return text_size


def dummy_func(doc):
    return doc
