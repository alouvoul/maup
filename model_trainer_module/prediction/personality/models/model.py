from sklearn.ensemble import RandomForestRegressor
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler


def model_training(x_train, y_train, **rf_params):
    rf = RandomForestRegressor(**rf_params)
    mms = MinMaxScaler()

    model = Pipeline([
        ('scaler', mms),
        ('clf', rf),
    ]).fit(x_train, y_train)
    return model


def dummy_func(doc):
    return doc