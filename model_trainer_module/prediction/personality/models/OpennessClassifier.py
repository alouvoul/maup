import pickle

from lime.lime_text import LimeTextExplainer

from model_trainer_module.prediction.personality.PersonalityInterpretator import PersonalityInterpretator
from model_trainer_module.prediction.personality.models.model import model_training
from model_trainer_module.prediction.utils.model_reader import model_reader
from model_trainer_module.prediction.utils.models_performance import performance_regression
from model_trainer_module.preprocess.ModelPreprocessor import ModelPreprocessor
from model_trainer_module.utils.path_utils import resources_path


class OpennessClassifier:
    filename = resources_path('models/personality/openness_classifier.sav')
    filename_explainer = resources_path('models/personality/openness_classifier_explainer.sav')
    model = None
    interpret = None
    class_name = 'OPEN'

    def __init__(self, x_train, y_train, x_test, y_test):
        # Initialize the explainer
        text_features = x_train.columns.tolist()

        self.interpret = PersonalityInterpretator(x_train=x_train, y_train=y_train, text_features=text_features, class_name=self.class_name)

        self.model = model_reader(self.filename)
        if self.model:
            return

        self.model = model_training(x_train, y_train, n_estimators=400, min_samples_split=3, min_samples_leaf=2,
                                    max_features='sqrt', max_depth=None, bootstrap=False)
        performance_regression(self.model.predict(x_test), y_test)
        # import shap
        # se = shap.TreeExplainer(self.model)  # , feature_perturbation="interventional", model_output="raw"
        # shap_values = se.shap_values(x_train)
        # shap.summary_plot(shap_values, features=x_train, max_display=10, plot_size=(14, 8))

        print("Persisting openness model...")
        pickle.dump(self.model, open(self.filename, "wb"))

    def predict_explain(self, data):
        return self.interpret.predict_explain(self.model, instance=data)
