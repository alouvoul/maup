import pymongo
import pandas as pd
from personality_features import get_personality_scores
from personality_features import get_lang_based_scores



# Example of how your dataframes should look like

# load your collection - change if you use other storage type
conn = pymongo.MongoClient('mongodb://localhost')
db = conn['userdb']
tweets_collection = db['tweets']

# creating tweets dataset --if you have tweets only/anonymized cases -- MUST contain user_id field
tweets = pd.json_normalize(tweets_collection.find({}, {'user.id_str':1, 'id_str':1, 'text':1, 'favorite_count':1, 'retweet_count':1, 'created_at':1}))
tweets.drop('_id', axis=1, inplace=True)
tweets.rename(columns={'user.id_str': 'user_id', 'id_str': 'tweet_id', 'created_at.$date': 'created_at'}, inplace=True)
tweets.created_at = pd.to_datetime(tweets.created_at ,unit='ms')
# #drop all retweets -- if need
# tweets = tweets.drop(tweets[tweets['text'].str.startswith('RT').apply(lambda x: 1 if x==True else 0) == 1].index)
# tweets.reset_index(inplace=True)

# creating users dataset --if you have full profiles
users = pd.json_normalize(tweets_collection.find({}, {"user.id_str":1, "user.name":1, "user.screen_name":1, "user.followers_count":1, "user.friends_count":1, "user.listed_count":1, "user.statuses_count":1,  "user.favourites_count":1, "user.description":1,"user.created_at":1, "user.location":1 }))
users.drop('_id', axis=1, inplace=True)
users.rename(columns={'user.id_str': 'twitter_id', 'user.name': 'name', 'user.screen_name': 'screen_name', 'user.followers_count': 'followers_count', 'user.friends_count': 'friends_count', 'user.listed_count': 'listed_count', 'user.statuses_count': 'statuses_count', 'user.favourites_count': 'favourites_count', 'user.description': 'description', 'user.created_at': 'created_at', 'user.location': 'location'}, inplace=True)
users.drop_duplicates('twitter_id', inplace=True)




# get scores with LANGUAGE - EMOTION - BEHAVIOR features
# we need all the attributes below retrieved from Twitter API
# for tweets -- user id, tweet id, text, feavorite count, retweet count, created at
# for users -- user id, name, screen name, followers count, friends count, listed count, statuses_count, favourites count, description, created at,  location
users_with_personality = get_personality_scores(users, tweets) #returns the initial df with more user attributes, emotion profiles and personality scores


# get scores with LANGUAGE ONLY features
# we need only user id and text of each tweets
# user1 - tweet1
# user1 - tweet2
# ....
# usern - tweetm
users_with_personality = get_lang_based_scores(tweets) #returns a df with user_id and personality scores
