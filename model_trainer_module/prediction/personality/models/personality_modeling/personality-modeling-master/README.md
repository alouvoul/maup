## Twitter-users personality profiling 

Karanatsiou, D., Sermpezis, P., Gruda, J., Kafetsios, K., Dimitriadis, I., & Vakali, A. (2020). My tweets bring all the traits to the yard: Predicting personality and relational traits in Online Social Networks. arXiv preprint arXiv:2009.10802.

### Need to know
*Twitter profiles must be already stored - no direct communication with Twitter API (example of required data is given)*

*Requires scikit 0.22rc2.post1*
