import pickle

import nltk
from sklearn.linear_model import LinearRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestRegressor

from model_trainer_module.feature_extraction.PosTagger import PosTagger
from model_trainer_module.feature_extraction.feature_extractors.PersonalityFeatureExtractor import \
    PersonalityFeatureExtractor

import pandas as pd

from imblearn.pipeline import Pipeline

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.pipeline import FeatureUnion
from sklearn.svm import SVR

from model_trainer_module.feature_extraction.feature_extractors.SentimentFeatureExtractor import \
    SentimentFeatureExtractor
from model_trainer_module.preprocess.ModelPreprocessor import ModelPreprocessor
from model_trainer_module.prediction.utils.model_reader import model_reader
from model_trainer_module.prediction.utils.models_performance import performance_regression
from model_trainer_module.utils.path_utils import resources_path


class TextModel:
    model = None
    lda = None
    model_filename = resources_path("models/personality/agreeableness_text_classifier.sav")
    dataset_filename = resources_path("dataset/personality/final_personality.csv")
    interpreter = None

    def __init__(self):
        # self.model = model_reader(self.model_filename)
        # if self.model is not None:
        #     return

        data = pd.read_csv(self.dataset_filename, index_col='id')

        # Train set
        x = data[[
            'tweets'
        ]]
        # Test set
        y = data[[
            'OPEN', 'CONS', 'EXTR', 'AGRE', 'NEUR'
        ]]

        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

        mp = ModelPreprocessor()
        train = mp.transform(x_train['tweets'])
        self.__train_model(train, y_train['NEUR'])

        test = mp.transform(x_test['tweets'])
        y_pred = self.model.predict(test)
        performance_regression(y_pred, y_test['NEUR'])

        print("Persisting Models...")
        # pickle.dump(self.model, open(self.model_filename, "wb"))

    def __train_model(self, x_train, y_train):
        rf = RandomForestRegressor
        # sf = SentimentFeatureExtractor()
        # svm = SVC(kernel='linear', gamma=1, C=1, probability=True)
        # nb = MultinomialNB(fit_prior=True, alpha=1)
        # lr = LogisticRegression(solver='newton-cg', penalty='l2', C=10)
        # estimators = [("svm", svm), ("rf", rf), ("nb", nb), ("lr", lr)]
        self.model = Pipeline([
            ('cv', CountVectorizer(strip_accents='unicode',
                                   analyzer="word",
                                   token_pattern=r"[\w\-:]+",
                                   preprocessor=dummy_func,
                                   tokenizer=dummy_func,
                                   # min_df=,
                                   max_df=0.85,
                                   ngram_range=(1, 2),
                                   lowercase=False)),
            # ('u1', FeatureUnion([
            #     ('features', Pipeline([
            #         ('cv', PersonalityFeatureExtractor())
            #     ])),
            #     ('vectorizer', Pipeline([
            #         ('cv', CountVectorizer(strip_accents='unicode',
            #                                analyzer="word",
            #                                token_pattern=r"[\w\-:]+",
            #                                preprocessor=dummy_func,
            #                                tokenizer=dummy_func,
            #                                # min_df=,
            #                                max_df=0.85,
            #                                ngram_range=(1, 2),
            #                                lowercase=False)),
            #     ])),
            # ])),
            ('clf', rf)]
        ).fit(x_train, y_train)

    def personality_of(self, text):

        return self.model.predict([text])


def dummy_func(doc):
    return doc


if __name__ == "__main__":
    text = TextModel()