import pickle
import numpy as np
import pandas as pd
from lime.lime_tabular import LimeTabularExplainer

from sklearn.model_selection import train_test_split

from model_trainer_module.utils.path_utils import resources_path

dataset_filename = resources_path("dataset/personality/final_personality.csv")


class PersonalityInterpretator:

    interpret_model = None
    filename_explainer = resources_path('models/personality/openness_classifier_explainer.sav')

    def __init__(self, x_train, y_train,  text_features, class_name):

        try:
            self.interpret_model = pickle.load(open(resources_path('models/personality/explain_personality__' + class_name + '.sav'), "rb"))
        except:
            self.interpret_model = LimeTabularExplainer(
                training_data=np.array(x_train),
                mode="regression",
                feature_names=text_features,
                class_names=class_name,
                training_labels=np.array(y_train),
                discretize_continuous=False
            )

    def predict_explain(self, model, instance=None, html_file=False, number_of_features=10):
        instance = instance.iloc[0, :]
        exp = self.interpret_model.explain_instance(instance, model.predict, num_features=20)

        feature_list = exp.as_list()
        new_list = []
        for feature in feature_list:
            new_list.append(
                (feature_full_names[feature[0]],
                feature[1])
            )

        if html_file:
            import random
            exp.as_pyplot_figure(label=1).show()
            exp.save_to_file(resources_path("explain\\_" + str(random.randint(0, 10000000)) + "_explain.html"))

        return exp.predicted_value, new_list[0:number_of_features]


feature_full_names = {
    'favourites_count': 'Total Favourites',
    'listed_count': 'Listed Count',
    'total_number_mentions': '#Mentions',
    'capital_letters': '#Capital letters',
    'statuses_count': '#Status updates',
    'followers_count': '#Followers',
    'no_of_links': '#URLS',
    'total_number_hashtags': '#Hashtags',
    'words_average_length': 'Length of words',
    'multi_period_count': '#Multi periods',
    'post_length': 'Post length',
    'period_count': 'Number of Periods',
    'sentiment': 'Sentiment value',
    'description': 'Description',
    'screen_name': 'Username Length',
    'negation': 'Negative words',
    'flooding': 'Multi-Character',
    'emojies': '#Emojies',
    'is_retweet': '#Retweets',
    'friends_count': '#friends',
}
