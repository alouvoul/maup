import pandas as pd

from model_trainer_module.prediction.personality.PersonalityClassifier import PersonalityClassifier
from model_trainer_module.utils.path_utils import resources_path


class Personality:
    classifier = None

    def __init__(self):
        print("|---- Initialize personality model")
        self.classifier = PersonalityClassifier()
        print("|---- Initialize sentiment model ended")

    def personality_of(self, data: pd.DataFrame):
        return self.classifier.personality_of(data)
