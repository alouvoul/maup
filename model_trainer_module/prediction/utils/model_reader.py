import os
import pickle


def model_reader(filename=None):

    if not filename:
        return None

    if os.path.exists(filename):
        try:
            with open(filename, "rb") as f:
                model = pickle.load(f)
            return model
        except Exception:
            print("Failed to load model. Resuming model creation...")
            return None


def model_writer(model, model_filename=None):
    with open(model_filename, "wb") as f:
        pickle.dump(model, f)
