import os
import pandas as pd


def read_file(filename, delimiter=','):
    file = None
    if os.path.exists(filename):
        try:
            file = pd.read_csv(filename, delimiter=delimiter)
            print("Read file: " + filename)
            return file
        except Exception:
            print("Failed to load file: " + filename)
    return None
