from sklearn import metrics


def performance(y_test, y_pred):
    accuracy = metrics.accuracy_score(y_test, y_pred)
    recall = metrics.recall_score(y_test, y_pred, average="macro")
    precision = metrics.precision_score(y_test, y_pred, average="macro")
    f1 = metrics.f1_score(y_test, y_pred, average="macro")
    print("\tAccuracy: %f" % accuracy)
    print("\tRecall: %f" % recall)
    print("\tPrecision: %f" % precision)
    print("\tF1: %f" % f1)


def performance_regression(y_test, y_pred):
    mae = metrics.mean_absolute_error(y_test, y_pred)
    mse = metrics.mean_squared_error(y_test, y_pred, squared=False)
    print("\tMAE: %f" % mae)
    print("\tmse: %f" % mse)
