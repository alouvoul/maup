import numpy as np
from sklearn.ensemble import RandomForestClassifier

n_estimators = [int(x) for x in np.linspace(start=200, stop=2000, num=10)]
# Number of features to consider at every split
max_features = ['auto', 'sqrt']
# Maximum number of levels in tree
max_depth = [int(x) for x in np.linspace(10, 110, num=11)]
max_depth.append(None)
# Minimum number of samples required to split a node
min_samples_split = [2, 5, 10]
# Minimum number of samples required at each leaf node
min_samples_leaf = [1, 2, 4]
# Method of selecting samples for training each tree
bootstrap = [True, False]  # Create the random grid


def get_rf_parameters():
    random_grid = {'n_estimators': n_estimators,
                   'max_features': max_features,
                   'max_depth': max_depth,
                   'min_samples_split': min_samples_split,
                   'min_samples_leaf': min_samples_leaf,
                   'bootstrap': bootstrap}
    return random_grid


def get_svm_parameters():
    return {
        'C': [1, 10, 100, 1000],
        'gamma': [1, 0.1, 0.001, 0.0001],
        'kernel': ['poly', 'rbf', 'sigmoid', 'linear'],
    }


def get_lr_parameters():
    return {
        'solver': ["newton-cg", "lbfgs", "liblinear", "sag", "saga"],
        'penalty': ["none", "l1", "l2", "elasticnet"],
        'C': [100, 10, 1.0, 0.1, 0.01],
    }


def get_voting_classifier_parameters():
    return {
        'rf__n_estimators': 800,
        'rf__max_features': 'sqrt',
        'rf__max_depth': 70,
        'rf__min_samples_split': 5,
        'rf__min_samples_leaf': 4,
        'rf__bootstrap': False,
    }


def get_nb_parameters():
    return {
        'alpha': (1, 0.1, 0.01, 0.001, 0.0001, 0.00001),
        'fit_prior': [True, False],
    }


def get_nn_parameters():
    return {
        'solver': ['lbfgs', 'sgd', 'adam'],
        'max_iter': [1000, 1100, 1200, 1300, 1400, 1500, 1600],
        'alpha': 10.0 ** -np.arange(1, 6),
        'hidden_layer_sizes': np.arange(10, 13),
    }


def get_ab_parameters():
    return {
        'n_estimators': [10, 50, 100, 500],
        'learning_rate': [0.0001, 0.001, 0.01, 0.1, 1.0],
    }


def get_classifier_chain_parameters():
    return {
        'estimator__n_estimators': n_estimators,
        'estimator__max_features': max_features,
        'estimator__max_depth': max_depth,
        'estimator__min_samples_split': min_samples_split,
        'estimator__min_samples_leaf': min_samples_leaf,
        'estimator__bootstrap': bootstrap
    }


def get_classifier_chain_regressor_parameters():
    return {
        'base_estimator__n_estimators': [int(x) for x in np.linspace(start=800, stop=2000, num=6)],
        'base_estimator__max_features': max_features,
        'base_estimator__max_depth': [int(x) for x in np.linspace(10, 90, num=11)],
        'base_estimator__min_samples_split': min_samples_split,
        'base_estimator__min_samples_leaf': min_samples_leaf,
        'base_estimator__bootstrap': bootstrap
    }

# TUNING PARAMETERS FOR VOTING

# 'rf__n_estimators': n_estimators,
# 'rf__max_features': max_features,
# 'rf__max_depth': max_depth,
# 'rf__min_samples_split': min_samples_split,
# 'rf__min_samples_leaf': min_samples_leaf,
# 'rf__bootstrap': bootstrap,
# 'svm__C': [1, 10, 100, 1000],
# 'svm__gamma': [1, 0.1, 0.001, 0.0001],
# 'svm__kernel': ['poly', 'rbf', 'sigmoid', 'linear'],
# 'nb__alpha': (1, 0.1, 0.01, 0.001, 0.0001, 0.00001),
# 'nn__solver': ['lbfgs'],
# 'nn__max_iter': [1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000],
# 'nn__alpha': 10.0 ** -np.arange(1, 10), 'hidden_layer_sizes': np.arange(10, 15),
# 'nn__random_state': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
