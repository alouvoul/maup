import math
from sklearn.preprocessing import MinMaxScaler


mm = MinMaxScaler()


def dict_values_log(dictionary: dict):
    min_value = min(dictionary.values())
    max_value = max(dictionary.values())

    for key, value in dictionary.items():
        dictionary[key] = formula(value, min_value, max_value)

    return dictionary


def formula(x, min_value, max_value):
    return 2 * ((x - min_value) / (max_value - min_value)) - 1