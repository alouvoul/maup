from imblearn.over_sampling import BorderlineSMOTE
from imblearn.ensemble import EasyEnsembleClassifier
from imblearn.pipeline import Pipeline
from imblearn.under_sampling import NearMiss


class Imbalancer:

    x = None
    y = None

    def __init__(self):

        steps = [
            ('o', BorderlineSMOTE(k_neighbors=7, kind="borderline-1")),
            ('u', EasyEnsembleClassifier(random_state=1))  # EasyEnsembleClassifier(random_state=1)
        ]
        self.pipeline = Pipeline(steps=steps)

    def fit(self, x, y=None):
        self.x = x
        self.y = y
        return self

    def transform(self, x, y):
        # transform the dataset
        new_x_train, new_y_train = self.pipeline.fit_resample(x, self.y)

        return new_x_train, new_y_train
