import pickle
import pandas as pd
from nltk.corpus import names
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import RandomizedSearchCV
from sklearn.pipeline import Pipeline

from model_trainer_module.prediction.utils.model_tuning_parameters import get_rf_parameters, get_svm_parameters, get_nb_parameters
from model_trainer_module.prediction.utils.models_performance import performance
from model_trainer_module.preprocess.ModelPreprocessor import ModelPreprocessor
from model_trainer_module.preprocess.preprocess import preprocess
from model_trainer_module.utils.path_utils import resources_path
from nltk.stem.porter import PorterStemmer

GENDER_FEMALE = 'female'
GENDER_MALE = 'male'
GENDER_OTHER = 1


class Gender_Classifier_1:
    model_filename1 = resources_path("models/gender/gender_classifier.sav")
    model1 = None
    train_df = None
    test_df = None
    stemmer = PorterStemmer()

    def __init__(self):
        self.males = [name.lower() for name in names.words('male.txt')]

        all_names = []
        for name in self.males:
            all_names.append(name)
            all_names.append(self.stemmer.stem(name))

        self.males = set(all_names)

        self.females = [name.lower() for name in names.words('female.txt')]

        all_names = []
        for name in self.females:
            all_names.append(name)
            all_names.append(self.stemmer.stem(name))

        self.females = set(all_names)

        self.__load_model1()

    def __load_model1(self):

        try:
            with open(self.model_filename1, "rb") as f:
                self.model1 = pickle.load(f)
                print("|    Load persisted model1")
            return
        except Exception:
            print("Failed to load model. Resuming model creation...")
        self.train_df = pd.read_csv(resources_path("dataset/gender/pan18-author-train-tweets-concat"), delimiter=",")
        self.test_df = pd.read_csv(resources_path("dataset/gender/pan18-author-test-tweets-concat"), delimiter=",")

        print("|----    Gender context model training ")
        from model_trainer_module.preprocess.GenderModelPreprocessor import GenderModelPreprocessor
        pr = GenderModelPreprocessor()
        train = pr.transform(self.train_df['Tweet'])
        self.__train_first_model(train, self.train_df['Gender'])

        test = pr.transform(self.test_df['Tweet'])
        y_pred = self.model1.predict(test)
        performance(y_pred, self.test_df['Gender'])

        print("Persisting first Model...")
        pickle.dump(self.model1, open(self.model_filename1, "wb"))

    def __train_first_model(self, x_train, y_train):

        rf = RandomForestClassifier(n_estimators=1000, min_samples_split=10, min_samples_leaf=1, max_features='auto',
                                    max_depth=80, bootstrap=False)
        svm = SVC(kernel='linear', gamma=1, C=1, probability=True)
        nb = MultinomialNB(fit_prior=True, alpha=1)
        lr = LogisticRegression(solver='newton-cg', penalty='l2', C=10)
        estimators = [("svm", svm), ("rf", rf), ("nb", nb), ("lr", lr)]

        self.model1 = Pipeline([
            ('cv', CountVectorizer(strip_accents='unicode',
                                   analyzer="word",
                                   token_pattern=r"[\w\-:]+",
                                   preprocessor=dummy_func,
                                   tokenizer=dummy_func,
                                   min_df=5,
                                   max_df=0.85,
                                   ngram_range=(1, 2))),
            # ('clf', rf),
            ('clf', rf),
        ]).fit(x_train, y_train)

    def detect_gender(self, user_name: str, posts: list):
        """

        :param user_name: A user's name in string format
        :param posts: List of posts
        :return: "M" if is a male, "F" if is a female, "O" if is other
        """
        name_gender = self.detect_gender_by_user_name(user_name)
        posts_gender = self.detect_gender_by_posts_texts(posts)

        return name_gender, posts_gender

    def __gender_of_token(self, name_token):
        if name_token in self.males and name_token not in self.females:
            # if male add 0
            return 0.0
        elif name_token in self.females and name_token not in self.males:
            # if female add 1
            return 1.0
        elif name_token in self.males and name_token in self.females:
            # if undetermined add 0.5
            return 0.5
        else:
            stemmed_name_token = self.stemmer.stem(name_token)

            if stemmed_name_token != name_token:
                return self.__gender_of_token(stemmed_name_token)

            # if undetermined add 0.5
            #   to avoid skewing prediction
            return 0.5

    def detect_gender_by_user_name(self, user_name):
        tokens = [t.strip().lower() for t in user_name.split()]
        prediction_sum = 0.0

        if tokens is None or len(tokens) == 0:
            return GENDER_OTHER

        # find gender for each token in user's name
        for name_token in tokens:
            prediction_sum += self.__gender_of_token(name_token)

        prediction = prediction_sum / len(tokens)

        if 0 <= prediction <= 0.45:
            return GENDER_MALE
        elif 0.45 < prediction < 0.55:
            return GENDER_OTHER
        elif 0.55 <= prediction <= 1.0:
            return GENDER_FEMALE

        return GENDER_OTHER

    def detect_gender_by_posts_texts(self, posts):
        prediction_sum = 0.0

        for post in posts:
            pred = self.model1.predict([post])

            if pred[0] == GENDER_MALE:
                prediction_sum += 0.0
            elif pred[0] == GENDER_FEMALE:
                prediction_sum += 1.0
            else:
                prediction_sum += 0.5

        if posts is None or len(posts) == 0:
            return GENDER_OTHER

        prediction = prediction_sum / len(posts)

        if 0 <= prediction <= 0.50:
            return GENDER_MALE
        else:
            return GENDER_FEMALE


def dummy_func(doc):
    return doc