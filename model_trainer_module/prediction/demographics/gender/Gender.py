import pandas as pd
from lime.lime_tabular import LimeTabularExplainer
from lime.lime_text import LimeTextExplainer

from model_trainer_module.feature_extraction.feature_extractors.GenderFeatureExtractor import GenderFeatureExtractor
from model_trainer_module.prediction import UserProfile
from model_trainer_module.prediction.demographics.gender.GenderClassifier import GenderClassifier
from model_trainer_module.prediction.demographics.gender.GenderClassifier2 import GenderClassifier2
from model_trainer_module.prediction.demographics.gender.Gender_Classifier_1 import Gender_Classifier_1
from model_trainer_module.preprocess.GenderModelPreprocessor import GenderModelPreprocessor

import math


class Gender:
    gfe = GenderFeatureExtractor()
    explain_model1 = None
    explain_model2 = None
    interpret_model = None
    lda = None

    def __init__(self):
        self.model1 = Gender_Classifier_1()

        self.model2 = GenderClassifier2().model2

    def gender_of(self, user, tweets: list):
        """

        :param user:
        :param tweets:
        :return: prediction(text), feature importance(list) text model, feature importance(list) content model
        """
        return self.predict(user, tweets)

    def predict(self, user, tweets: list):
        """
        The given string is used to predict the gender of the individual. The model that have trained used in order to
        make the predictions.
        :param user: user data
        :param tweets: A string sentence
        :return: "M" if is a male, "F" if is a female, "O" if is other,
        """

        total_text = pd.DataFrame(tweets, columns=['Tweets'])
        mp = GenderModelPreprocessor()
        preprocessed_total_text = mp.transform(total_text['Tweets'])
        preprocessed_total_text.dropna(inplace=True)
        total_tweets = ""
        for i in preprocessed_total_text:
            total_tweets = total_tweets + " " + " ".join(i)

        instance = [total_tweets.split(" ")]
        y_pred = self.model1.model1.predict(instance)

        user['text_model'] = y_pred
        user = user.replace('M', UserProfile.GENDER_MALE)
        user = user.replace('F', UserProfile.GENDER_FEMALE)
        user = user.replace('female', UserProfile.GENDER_FEMALE)
        user = user.replace('male', UserProfile.GENDER_MALE)
        user['Tweets'] = total_text
        user['ID'] = None
        user = self.gfe.transform(user)

        # y_pred1 = self.model2.predict(user)
        # y_pred1 = "Male" if y_pred == 0 else "Female"

        model_1_interpret = self.interpret(total_tweets, self.model1)

        model_2_interpret = self.interpret_model_2(user.values[0], self.model2)

        return y_pred, model_1_interpret, model_2_interpret

    def interpret(self, instance, model_filename, html_file=False):

        explainer = LimeTextExplainer(class_names=['male', 'female'])
        explanation = explainer.explain_instance(instance, self.model1.model1.predict_proba, num_features=40)
        feature_list = explanation.as_list()
        feature_list_for_predicted_gender = []
        for i in feature_list:
            temp = i[1] * math.pow(10, 30)
            if explanation.predict_proba[1] > 0.5:
                if temp < 0:
                    feature_list_for_predicted_gender.append((i[0], -1 * temp))
            else:
                if temp > 0:
                    feature_list_for_predicted_gender.append((i[0], temp))

        if html_file:
            explanation.as_pyplot_figure(label=1).show()
        print("| Interpret of gender first model finished -\n\n")
        return feature_list_for_predicted_gender

    def interpret_model_2(self, instance, html_file=False):

        train_data = pd.read_csv(resources_path("dataset/gender/renew_gender.csv"), delimiter=",").dropna(axis=0)
        train_data.drop(train_data.columns[train_data.columns.str.contains('unnamed', case=False)], axis=1,
                        inplace=True)
        train_data = train_data.replace('M', UserProfile.GENDER_MALE)
        train_data = train_data.replace('F', UserProfile.GENDER_FEMALE)
        train_data = train_data.replace('female', UserProfile.GENDER_FEMALE)
        train_data = train_data.replace('male', UserProfile.GENDER_MALE)

        train_data = np.array(self.gfe.transform(train_data))
        text_features = self.gfe.get_feature_names()
        class_names = [UserProfile.GENDER_MALE, UserProfile.GENDER_FEMALE]

        self.interpret_model = LimeTabularExplainer(
            training_data=train_data,
            mode="classification",
            feature_names=text_features,
            class_names=class_names,
            # training_labels=training_labels,
            discretize_continuous=False
        )

        # features = self.gfe.transform(instance).values[0]
        exp = self.interpret_model.explain_instance(instance, self.model2.predict_proba, num_features=10)
        # if exp.predict_proba[0] > exp.predict_proba[1]:
        #     feature_list = exp.as_list(label='0')
        # else:
        #     feature_list = exp.as_list(label='1')
        feature_list = exp.as_list()
        feature_list_for_predicted_gender = []

        feature_list = exp.as_list()


        for i in feature_list:
            temp = i[1]
            if temp < 0:
                if abs(temp) * math.pow(10, 16) > 0:
                    feature_list_for_predicted_gender.append(
                        (change_feature_names_for_UI(i[0]), -1 * math.log(abs(temp) * math.pow(10, 16))))
                else:
                    feature_list_for_predicted_gender.append(
                        (change_feature_names_for_UI(i[0]), -1))
            else:
                if temp * math.pow(10, 16) > 0:
                    feature_list_for_predicted_gender.append(
                        (change_feature_names_for_UI(i[0]), math.log(temp * math.pow(10, 16))))
                else:
                    feature_list_for_predicted_gender.append(
                        (change_feature_names_for_UI(i[0]), 1))

        if html_file:
            import random
            print("Lime explanation: ")
            exp.as_pyplot_figure(label=1).show()
            exp.save_to_file(resources_path("explain\\_" + str(random.randint(0, 10000000)) + "_explain.html"))

        return feature_list_for_predicted_gender


def dummy_func(doc):
    return doc


def change_feature_names_for_UI(feature):
    return feature_full_names[feature]


feature_full_names = {
    'number_of_mentions': '#Mentions',
    'number_of_hashtags': '#Hashtags',
    'number_of_urls': '#URLS',
    "flooding": 'Multi-Character',
    "total_number_punctuations": '# marks',
    "words_average_length": 'Post length',
    "avg_sentiment_pos": 'Positive sentiment',
    "avg_sentiment_neg": 'negative sentiment',
    "post_length": 'Post length',
    "number_of_emojies": '#Emojies',
    "number_of_stopwords": '#stopwords',
    'description_length': 'Description',
    'years_active': 'Years Active',
    'followers_count': '#Followers',
    'friends_count': '#Friends',
    'statuses_count': '#Status updates',
    'favourites_count': '#Favourites',
    'verified': 'Verified',
    'average_likes': 'Avg likes',
    'text_model': 'Text model',
}
