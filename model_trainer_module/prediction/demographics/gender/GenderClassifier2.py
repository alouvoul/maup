import pickle
import pandas as pd

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import RobustScaler

from model_trainer_module.feature_extraction.feature_extractors.GenderFeatureExtractor import GenderFeatureExtractor
from model_trainer_module.prediction.demographics.gender.Gender_Classifier_1 import Gender_Classifier_1
from model_trainer_module.prediction.utils.model_tuning_parameters import get_rf_parameters
from model_trainer_module.prediction.utils.models_performance import performance
from model_trainer_module.utils.path_utils import resources_path


class GenderClassifier2:

    model2 = None
    model_filename1 = resources_path("models/gender/gender_classifier.sav")
    model_filename2 = resources_path("models/gender/gender_classifier_unsupervised.sav")

    def __init__(self):
        self.__load_model2()

    def __load_model2(self):
        try:
            with open(self.model_filename2, "rb") as f:
                self.model2 = pickle.load(f)
                print("|    Load persisted model1")
            return
        except Exception:
            print("Failed to load model. Resuming model creation...")

        print("|----    Gender second model training ")
        content_dataset = pd.read_csv(resources_path("dataset/gender/renew_gender.csv"), delimiter=",").dropna(axis=0)
        content_dataset.drop(content_dataset.columns[content_dataset.columns.str.contains('unnamed', case=False)],
                             axis=1, inplace=True)

        content_dataset['text_model'] = content_dataset['text_model'].map({'female': 1, 'male': 0})
        content_dataset['Gender'] = content_dataset['Gender'].map({'F': 1, 'M': 0})
        performance(content_dataset['Gender'], content_dataset['text_model'])

        # 1. Predict the second dataset and add to the dataset
        self.check_the_second_dataset(content_dataset)

        # 2. Train the model from the output
        x_train, x_test, y_train, y_test = train_test_split(content_dataset.loc[:, content_dataset.columns != 'Gender'],
                                                            content_dataset.loc[:, "Gender"], test_size=0.30)
        gender_feature_extractor = GenderFeatureExtractor()
        self.__train_second_model(gender_feature_extractor.transform(x_train), y_train)

        y_pred = self.model2.predict(gender_feature_extractor.transform(x_test))

        performance(y_pred, y_test)
        print("Persisting second Model...")
        pickle.dump(self.model2, open(self.model_filename2, "wb"))

    def __train_second_model(self, x_train, y_train):
        x_train = x_train.replace('M', 0)
        x_train = x_train.replace('F', 1)
        x_train['verified'] = x_train['verified'].astype(int)

        self.model2 = Pipeline([
            ('scaler', RobustScaler()),
            ('clf', RandomForestClassifier(n_estimators=2000, min_samples_split=5, min_samples_leaf=2, max_features='auto',
                                    max_depth=50, bootstrap=True)),
        ]).fit(x_train, y_train)

    def check_the_second_dataset(self, content_dataset):
        """
        Checks if the second dataset contains the predictions from the first (text model).
        :param content_dataset:
        :return:
        """
        if 'text_model' not in content_dataset:
            try:
                with open(self.model_filename1, "rb") as f:
                    model1 = pickle.load(f)
                    print("|    Load persisted model 2")
            except:
                model1 = Gender_Classifier_1().model1
                print("Failed to load model. Resuming model creation...")

            predicted_feature = model1.predict(content_dataset['Tweets'])
            content_dataset['text_model'] = predicted_feature
            content_dataset.to_csv(resources_path("dataset/gender/renew_gender.csv"))

