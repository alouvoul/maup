import pandas as pd
import numpy as np
import json
import requests


class GeoLocation:

    location = None

    def geocode(self, location):

        URL = "https://geocode.search.hereapi.com/v1/geocode"
        api_key = 'LHAyyGMLIMuhN0b6c7i-A6k4kB4eEHgTAnjWC-pk01M'  # Acquire from developer.here.com

        loc = []
        coords = []
        self.location = location

        PARAMS = {'apikey': api_key, 'q': self.location}
        r = requests.get(url=URL, params=PARAMS)
        data = r.json()
        try:
            latitude = data['items'][0]['position']['lat']
            longitude = data['items'][0]['position']['lng']
            coords.append(latitude)
            coords.append(longitude)
            loc.append(coords)
        except IndexError:
            print('Error while geocoding')

        return loc

    # TODO find sum of location