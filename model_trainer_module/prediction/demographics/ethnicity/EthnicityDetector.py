from langdetect import detect_langs


class EthnicityDetector:

    def ethnicity_detect(self, text):
        """
        Method that checks based on vocabulary the language.
        :param text: String
        :return: List of languages detected with weight values
        """
        ethnicity = detect_langs(text)
        languages_dict = {}
        for lang in ethnicity:
            l = str(lang)
            key = l.split(':')[0]
            value = l.split(':')[1]
            languages_dict[key.upper()] = str(round(float(value), 2) * 100)
        return languages_dict


language = {
    'af': 'Afrikaans',
    'ar': 'Arabic',
    'bg': 'Bulgarian',
    'bn': 'Bengali',
    'ca': 'Catalan',
    'cs': 'Czech',
    'cy': 'Welsh',
    'da': 'Danish',
    'de': 'German',
    'el': 'Greek',
    'en': 'English',
    'es': 'Spanish',
    'et': 'Estonian',
    'fa': 'Persian',
    'fi': 'Finnish',
    'fr': '	French',
    'gu': 'Gujarati',
    'he': 'Hebrew',
    'hi': 'Hindi',
    'hr': 'Croatian',
    'hu': 'Hungarian',
    'it': 'Italian',
    'ja': 'Japanese',
    'kn': 'Kannada',
    'ko': 'Korean',
    'lt': '	Lithuanian',
    'lv': 'Latvian',
    'mk': 'Macedonian',
    'ml': 'Malayalam',
    'mr': 'Marathi',
    'ne': 'Nepali',
    'nl': 'Dutch',
    'no': 'Norwegian',
    'pa': 'Punjabi',
    'pl': 'Polish',
    'pt': 'Portuguese',
    'ro': 'Romanian',
    'ru': 'Russian',
    'sk': 'Slovak',
    'sl': '	Slovenian',
    'so': 'Somali',
    'sq': '	Albanian',
    'sv': 'Swedish',
    'sw': 'Swahili',
    'ta': '	Tamil',
    'te': 'Telugu',
    'th': 'Thai',
    'tl': 'Tagalog',
    'tr': '	Turkish',
    'uk': 'Ukrainian',
    'ur': 'Urdu',
    'vi': 'Vietnamese'
}
