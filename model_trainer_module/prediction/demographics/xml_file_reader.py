import xml.etree.ElementTree as ET
import pandas as pd

from model_trainer_module.utils.path_utils import resources_path


# This code used to convert XML files to CSV for age and gender datasets.

def file_read(filepath):
    etree = ET.parse(source=filepath)
    tweets = etree.findall('.//document')
    tweet_list = []
    for tweet in tweets:
        tweet_list.append(tweet.text.rstrip())
    return tweet_list


def get_data(folder, columns, column):

    truth_file = pd.read_csv(folder + '/truth.txt', sep=":::", names=columns, engine='python')
    truth_file = truth_file.loc[:, ["user_id", column]]
    x_train = []
    y_train = []
    for user in truth_file.index:
        user_tweets = file_read(folder + "/text/" + str(truth_file.loc[user, 'user_id']) + ".xml")
        all_tweets = ""
        for tweet in user_tweets:
            all_tweets = all_tweets + " " + tweet
        x_train.append(all_tweets)
        y_train.append(truth_file.iloc[user, 1])
    return x_train, y_train


def train_test_data(train_filename, test_filename, write_train, write_test, columns_of_file, column):
    x_train, y_train = get_data(train_filename, columns_of_file, column)
    x_test, y_test = get_data(test_filename, columns_of_file, column)
    df = pd.DataFrame({
        "Tweet": x_train,
        column: y_train
    })
    df.to_csv(resources_path(write_train), index=False)
    df = pd.DataFrame({
        "Tweet": x_test,
        column: y_test
    })
    df.to_csv(resources_path(write_test), index=False)
    # truth_file_test = pd.read_csv('resources/pan15-author-test' + '/truth.txt', sep=":::", names=columns, engine='python')
    # truth_file_test = truth_file_test.loc[:, ["user_id", column]]
    # x_test = []
    # y_test = []
    # for user in truth_file_test.index:
    #     user_tweets = file_read(
    #         'resources/pan15-author-test/' + str(truth_file_test.loc[user, 'user_id']) + ".xml")
    #     for tweet in user_tweets:
    #         preprocessed_text = preprocess(tweet)
    #         if not preprocessed_text:
    #             continue
    #         tweet = preprocessed_text
    #
    #         x_test.append(tweet)
    #         y_test.append(truth_file_test.iloc[user, 1])
    return x_train, x_test, y_train, y_test


if __name__ == "__main__":
    # columns = ["user_id", "gender", "age", "1", "2", "3", "4", "5"]
    # train_filename = resources_path('dataset/gender/pan15-author-train')
    # test_filename = resources_path('dataset/gender/pan15-author-test')
    # write_train = 'dataset/gender/pan15-author-train-tweets-concat'
    # write_test = 'dataset/gender/pan15-author-test-tweets-concat'

    columns = ["user_id", "Gender"]
    train_filename = resources_path('dataset/gender/pan18-author-profiling-train')
    test_filename = resources_path('dataset/gender/pan18-author-profiling-test')
    write_train = 'dataset/gender/pan18-author-train-tweets-concat'
    write_test = 'dataset/gender/pan18-author-test-tweets-concat'

    train_test_data(train_filename, test_filename, write_train, write_test, columns, "Gender")

