import pickle

from imblearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer

from model_trainer_module.prediction.utils.model_reader import model_reader
from model_trainer_module.prediction.utils.models_performance import performance

from model_trainer_module.preprocess.ModelPreprocessor import ModelPreprocessor
from model_trainer_module.utils.path_utils import resources_path
from lime.lime_text import LimeTextExplainer


class AgeClassifier:
    model_filename = resources_path("models/age/interpret_age_classifier.sav")
    model = None
    dataset_filename = resources_path("dataset/age/pan15-author-train-tweets-concat")
    test_dataset_filename = resources_path("dataset/age/pan15-author-test-tweets-concat")
    mp = ModelPreprocessor()

    def __init__(self):

        # Train the dataset with the larger dataset
        self.model = model_reader(self.model_filename)
        if self.model:
            return

        dataset = read_file(self.dataset_filename)
        test_dataset = read_file(self.test_dataset_filename)

        # x_train, x_test, y_train, y_test = train_test_split(dataset['Tweet'], test_dataset['Age'], test_size=0.30)
        self.__train_model(x_train=dataset['Tweet'], y_train=dataset['Age'])

        print("|----    Age interpret model training ")
        performance(self.model.predict(self.mp.transform(test_dataset['Tweet'])), test_dataset['Age'])

        print("Persisting age interpret Model...")
        pickle.dump(self.model, open(self.model_filename, "wb"))

    def __train_model(self, x_train, y_train):
        """
        Method that used to train a ML model. if there is no model trained then this is used to train the model that is
        specified in the Pipeline.
        :param x_train: Dataframe with the text
        :param y_train: Dataframe with the labels
        """
        x_train = self.mp.transform(x_train)
        rf = RandomForestClassifier(n_estimators=200, min_samples_split=5, min_samples_leaf=2,
                               max_features="auto", max_depth=90, bootstrap=False)

        self.model = Pipeline([
            ('cv', CountVectorizer(strip_accents='unicode',
                                   analyzer="word",
                                   preprocessor=dummy_func,
                                   tokenizer=dummy_func,
                                   min_df=3,
                                   max_df=0.85,
                                   ngram_range=(1, 2),
                                   lowercase=False)),
            ('clf', rf),  # VotingClassifier(estimators, voting='soft', n_jobs=1)
        ]).fit(x_train, y_train)

    def age_of_post(self, post):
        """
        Method that text preprocessed and returns the probabilities per class.

        :param post:
        :return: probabilities per class, feature_list
        """
        # preprocessed_posts = preprocess(post)
        preprocessed_posts = self.mp.transform(post)
        return self.predict_interpret(preprocessed_posts)

    def predict_interpret(self, instance):
        instance = " ".join(instance)
        class_names = ['18-24', '25-34', '35-49', '50-XX']
        explainer = LimeTextExplainer(class_names=class_names)
        explanation = explainer.explain_instance(instance, self.model.predict_proba, num_features=40, labels=[0, 1, 2, 3])
        feature_list = explanation.as_list(label=0)
        probabilities = explanation.predict_proba
        probs = {
            '18-24': probabilities[0],
            '25-34': probabilities[1],
            '35-49': probabilities[2],
            '50-XX': probabilities[3],
        }
        print("| Interpret of gender first model finished -\n\n")
        return probs, feature_list


def dummy_func(doc):
    return doc


if __name__ == "__main__":
    import pandas as pd
    from model_trainer_module.prediction.utils.file_reader import read_file
    train = pd.read_csv(resources_path("dataset/age/tweet_age_train.csv"), delimiter=",")
    train.drop(['ID'], axis=1, inplace=True)
    # y.drop(['h'], axis=1, inplace=True)
    mp = ModelPreprocessor()
    train1 = mp.transform(train['Tweet'])
    cv = CountVectorizer(strip_accents='unicode',
                         analyzer="word",
                         preprocessor=dummy_func,
                         tokenizer=dummy_func,
                         min_df=7,
                         max_df=0.85,
                         ngram_range=(1, 2),
                         lowercase=False)
    x = cv.fit_transform(train1)
    # print(cv.)
    # print(cv.)
    #
    # print("|----    Age interpret model training ")
    #
    # y_pred = self.model.predict(test_dataset['Tweet'])
    # mp = ModelPreprocessor()
    # x_train = mp.transform(x_train)
    # performance(y_pred, test_dataset['Age'])




