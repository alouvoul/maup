import random

from model_trainer_module.prediction.demographics.age.AgeClassifier import AgeClassifier
from model_trainer_module.prediction.UserProfile import AGE_18_24, AGE_25_34, AGE_35_49, AGE_50_XX


class Age:

    classifier = None

    def __init__(self):
        print("|---- Initialize age classifier")
        self.classifier = AgeClassifier()
        print("|---- Initialize age classifier ended")

    def detect_age_list(self, tweets):
        """
        Used as factory class to get the tweets in a single instance for the predictions. For each class round the
        number for specific decimal points and then return the prediction along with the explainability.
        :param tweets: List of tweets
        :return: List of percentage per age group, explanation words
        """
        tweets_final = ''
        for tweet in tweets:
            tweets_final = tweets_final + ' ' + tweet

        prediction, explanation = self.classifier.age_of_post(tweets_final)
        for key in prediction:
            # rounding to K using round()
            prediction[key] = round(prediction[key], 2)
        return prediction, explanation

    def detect_age(self, tweets):
        """
        Detect the age of a user based a list of post texts. To be used need for the ML to be trained using list of
        tweets and not single concatenated tweets.
        :param tweets: a list of str - posts content
        :return: detected age string constant
        """
        count_18_24 = 0
        count_25_34 = 0
        count_35_49 = 0
        count_50_xx = 0

        prediction, explanation = self.classifier.age_of_post(tweets)
        for pred in prediction:
            # increase respective count
            if pred == AGE_18_24:
                count_18_24 += 1
            elif pred == AGE_25_34:
                count_25_34 += 1
            elif pred == AGE_35_49:
                count_35_49 += 1
            elif pred == AGE_50_XX:
                count_50_xx += 1

        # find the max count
        max_count = max(count_18_24, count_25_34, count_35_49, count_50_xx)

        # find the age with max count - best effort if all equal start from lowest to highest
        # since lower ages are more likely to use social media
        if max_count == count_18_24:
            result = AGE_18_24
        elif max_count == count_25_34:
            result = AGE_25_34
        elif max_count == count_35_49:
            result = AGE_35_49
        elif max_count == count_50_xx:
            result = AGE_50_XX

        return result, explanation

