import pandas as pd

from model_trainer_module.feature_extraction.PosTagger import PosTagger
from model_trainer_module.prediction.utils.file_reader import read_file
from model_trainer_module.prediction.utils.model_reader import model_reader
from model_trainer_module.utils.path_utils import resources_path

from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import NearMiss
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import f1_score, accuracy_score
from sklearn.pipeline import FeatureUnion
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from imblearn.pipeline import Pipeline

from model_trainer_module.feature_extraction.feature_extractors.AgeFeatures import AgeFeatures
from model_trainer_module.preprocess.ModelPreprocessor import ModelPreprocessor


class SemiSupervisedAgeModel:
    model_filename = resources_path("models/age/age_classifier.sav")
    train_dataset = resources_path("dataset/age/pan15-author-train-tweets-concat")
    test_dataset = resources_path("dataset/age/pan15-author-test-tweets-concat")
    semi_supervised_dataset = resources_path("dataset/age/labeled_by_model")

    def __init__(self):
        self.model = model_reader(self.model_filename)
        if self.model:
            return
        train_df = pd.read_csv(self.train_dataset, delimiter=",")
        test_df = pd.read_csv(self.test_dataset, delimiter=",")

        print("|----    Age semi-supervised model labeling-expanding the dataset ")
        labeled_file = read_file(self.semi_supervised_dataset)
        if labeled_file is None:
            df = train_df.drop_duplicates().groupby('Age').count()
            print(df)
            df = test_df.drop_duplicates().groupby('Age').count()
            print(df)
            labeled_file = self.__semi_supervised(
                train_df['Tweet'],
                test_df['Tweet'],
                train_df['Age'],
                test_df['Age']
            )
        print("|----    Age semi-supervised model Finished -- ")

    def __train_model(self, x_train, y_train, os_strategy, us_strategy):
        """
        This model used to label a dataset in order to extend the PAN-15.
        :param x_train:
        :param y_train:
        :param os_strategy:
        :param us_strategy:
        :return:
        """
        svm = SVC(kernel='poly', gamma=1, C=1, probability=True)
        rf = RandomForestClassifier(n_estimators=200, min_samples_split=5, min_samples_leaf=2, max_features="auto",
                                    max_depth=90, bootstrap=False)
        nb = MultinomialNB(alpha=1e-05)
        lr = LogisticRegression(solver="newton-cg", penalty='none', C=100)
        # create estimators list
        estimators = [("svm", svm), ("rf", rf), ("nb", nb), ("lr", lr)]

        self.model = Pipeline([
            ('u1', FeatureUnion([
                # ('pos_tag', Pipeline([
                #     ('pp', ModelPreprocessor()),
                #     ('pos', PosTagger()),
                # ])),
                ('vectorizer', Pipeline([
                    # ('pp', ModelPreprocessor()),
                    ('cv', CountVectorizer(strip_accents='unicode',
                                           analyzer="word",
                                           token_pattern=r"[\w\-:]+",
                                           preprocessor=dummy_func,
                                           tokenizer=dummy_func,
                                           min_df=7,
                                           # max_df=0.85,
                                           ngram_range=(1, 2),
                                           lowercase=False)
                     ),
                ])),
                ('text_features', Pipeline([
                    ('text', AgeFeatures()),
                ])),
            ])),
            ('os', SMOTE(sampling_strategy=os_strategy)),
            ('us', NearMiss(version=3, sampling_strategy=us_strategy)),
            ('clf', VotingClassifier(estimators, voting='soft', n_jobs=1)),
        ]).fit(x_train, y_train)

    def __semi_supervised(self, x_train, x_test, y_train, y_test):
        # Initiate iteration counter
        iterations = 0

        second_dataset = pd.read_csv(resources_path("dataset/gender/gender.csv"), delimiter=",", squeeze=True)
        x_unlabeled = second_dataset['Tweets'].dropna()

        # Containers to hold f1_scores and # of pseudo-labels
        train_f1s = []
        test_f1s = []
        pseudo_labels = []
        # Initial sampling
        os_strategy = {"18-24": 60, "25-34": 60, "35-49": 40, "50-XX": 40}
        us_strategy = {"18-24": 40, "25-34": 40, "35-49": 40, "50-XX": 30}
        # Assign value to initiate while loop
        high_prob = [1]
        number_of_iters = 0
        while len(high_prob) > 0 and number_of_iters < 7:
            number_of_iters = number_of_iters + 1
            # Fit classifier and make train/test predictions
            self.__train_model(x_train, y_train, os_strategy=os_strategy, us_strategy=us_strategy)
            y_hat_train = self.model.predict(x_train)
            y_hat_test = self.model.predict(x_test)

            # Calculate and print iteration # and f1 scores, and store f1 scores
            train_f1 = f1_score(y_train, y_hat_train, average="macro")
            test_f1 = f1_score(y_test, y_hat_test, average="macro")
            test_accuracy = accuracy_score(y_test, y_hat_test)
            print(f"Iteration {iterations}")
            print(f"Train f1: {train_f1}")
            print(f"Test f1: {test_f1}")
            print(f"Test accuracy: {test_accuracy}")
            train_f1s.append(train_f1)
            test_f1s.append(test_f1)

            # Generate predictions and probabilities for unlabeled data
            print(f"Now predicting labels for unlabeled data...")

            pred_probs = self.model.predict_proba(x_unlabeled)
            preds = self.model.predict(x_unlabeled)
            prob_0 = pred_probs[:, 0]
            prob_1 = pred_probs[:, 1]
            prob_2 = pred_probs[:, 2]
            prob_3 = pred_probs[:, 3]

            # Store predictions and probabilities in data frame
            df_pred_prob = pd.DataFrame([])
            df_pred_prob['preds'] = preds
            df_pred_prob['prob_0'] = prob_0
            df_pred_prob['prob_1'] = prob_1
            df_pred_prob['prob_2'] = prob_2
            df_pred_prob['prob_3'] = prob_3
            df_pred_prob.index = x_unlabeled.index

            PROBABILITY_BARRIER = 0.75
            # Separate predictions with > PROBABILITY_BARRIER probability
            high_prob = pd.concat([df_pred_prob.loc[df_pred_prob['prob_0'] > PROBABILITY_BARRIER],
                                   df_pred_prob.loc[df_pred_prob['prob_1'] > PROBABILITY_BARRIER],
                                   df_pred_prob.loc[df_pred_prob['prob_2'] > PROBABILITY_BARRIER],
                                   df_pred_prob.loc[df_pred_prob['prob_3'] > PROBABILITY_BARRIER]], axis=0)

            print(f"{len(high_prob)} high-probability predictions added to training data.")

            pseudo_labels.append(len(high_prob))

            # Add pseudo-labeled data to training data
            x_train = pd.concat([x_train, x_unlabeled.loc[high_prob.index]], axis=0)
            y_train = pd.concat([y_train, high_prob.preds])

            # Drop pseudo-labeled instances from unlabeled data
            x_unlabeled = x_unlabeled.drop(index=high_prob.index)
            print(f"{len(x_unlabeled)} unlabeled instances remaining.\n")

            os_strategy, us_strategy = self.__get_sampling_strategy(y_train)
            # Update iteration counter
            iterations += 1
        x_train.to_csv(resources_path('dataset/age/labeled_by_model_x.csv'))
        y_train.to_csv(resources_path('dataset/age/labeled_by_model_y.csv'))
        x = pd.DataFrame([x_train], columns=['Tweets'])
        y = pd.DataFrame([y_train], columns=['Age'])

        df = pd.join(x, y)
        df.to_csv(self.semi_supervised_dataset)
        return df

    def __get_sampling_strategy(self, data):
        df = data.groupby(data).count()
        maximum = max(df.values.astype(int))
        minimum = min(df.values.astype(int))
        # os_rate = int(0.8 * maximum) if 0.8 * maximum > minimum else maximum
        # us_rate = int(0.8 * maximum) if 0.8 * maximum > minimum else minimum
        os_rate = 1.3
        us_rate = 0.8
        os_strategy = {
            "18-24": df["18-24"] * os_rate,
            "25-34": df["25-34"] * os_rate,
            "35-49": df["35-49"] * os_rate,
            "50-XX": df["50-XX"] * os_rate
        }
        us_strategy = {
            "18-24": df["18-24"] * us_rate,
            "25-34": df["25-34"] * us_rate,
            "35-49": df["35-49"] * us_rate,
            "50-XX": df["50-XX"]
        }

        for key, value in os_strategy.items():
            os_strategy[key] = int(value)

        for key, value in us_strategy.items():
            us_strategy[key] = int(value)
        return os_strategy, us_strategy


def dummy_func(doc):
    return doc

if __name__ == '__main__':
    sm = SemiSupervisedAgeModel()