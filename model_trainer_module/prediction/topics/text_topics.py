import nltk
from nltk import ngrams


def extract_hashtags(text):
    hashtag_list = []

    for word in text.split():
        if word[0] == '#':
            hashtag_list.append(word[1:])

    return hashtag_list


def list_occurences(tag_cloud):
    """
    A list of words that will summarize to dictionary
    :param tag_cloud: List of occurences
    :return: Dict of occurrences with the sum number
    """
    tag_cloud_occurences_dict = {}

    for tag in tag_cloud:
        if tag in tag_cloud_occurences_dict.keys():
            tag_cloud_occurences_dict[tag] += 1
        else:
            tag_cloud_occurences_dict[tag] = 1
    return tag_cloud_occurences_dict


def get_bigrams(text):
    token = nltk.word_tokenize(text)
    bigrams = list(ngrams(token, 2))
    return bigrams
