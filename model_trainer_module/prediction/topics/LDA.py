import gensim
import pickle
import pyLDAvis
import pyLDAvis.gensim_models   # don't skip this
import pandas as pd
import matplotlib.pyplot as plt
from gensim import corpora, models
from nltk.corpus import stopwords

from model_trainer_module.prediction.utils.model_reader import model_reader
from model_trainer_module.preprocess.preprocess import preprocess
from model_trainer_module.utils.path_utils import resources_path
from model_trainer_module.prediction.utils.file_reader import read_file

stop_words = set(stopwords.words('english'))
visualize_topics = True


class LDA:
    dictionary_filename = resources_path("models/topics/dictionary.sav")
    model_filename = resources_path("models/topics/topic_classifier.sav")
    dataset_filename = resources_path("dataset/topics/topics.txt")

    dictionary = None
    model = None

    def __init__(self):
        pass
        # self.dictionary = model_reader(self.dictionary_filename)
        # self.model = model_reader(self.model_filename)

        # if self.model is not None:
        #     return
        #
        # self.lda_topics()

    def __tokens_of(self, text):
        discard = ["url", "amp"]

        for stopword in stop_words:
            discard.append(stopword)

        tokens = preprocess(text)
        tokens = [token for token in tokens if
                  len(token) > 2 and "_" not in token and token not in discard and token is not None]

        return tokens

    def lda_topics(self, tweets):
        """
        This function performs LDA analysis on the tweets and extracts topics.
        Simultaneously, it also visualises the topics based on the PyLDAVis library and saves the result as an HTML file
        :return:
        """
        global visualize_topics
        # tweets = read_file(self.dataset_filename, delimiter='\t')
        if not tweets:
            return None

        texts = []
        for tweet in tweets:

            tokens = self.__tokens_of(str(tweet))

            if len(tokens) < 1:
                continue

            texts.append(tokens)
        num_topics = 5
        self.dictionary = gensim.corpora.Dictionary(texts)

        self.dictionary.filter_extremes(no_below=0, no_above=0.6, keep_n=100000)
        bow_corpus = [self.dictionary.doc2bow(doc) for doc in texts]
        tfidf = models.TfidfModel(bow_corpus)
        # corpus_tfidf = tfidf[bow_corpus]
        self.model = gensim.models.LdaMulticore(bow_corpus, num_topics=num_topics, id2word=self.dictionary, passes=2, workers=2)

        topics = []

        for idx, topic in self.model.print_topics(-1):

            topic_dict = dict()
            topic_dict["topic_id"] = idx
            topic_dict["topic_name"] = "topic_" + str(idx)
            topic_dict["significance"] = num_topics - idx

            words = topic.split(" + ")
            topic_terms = []

            for word in words:
                parts = word.replace("\"", "").split("*")
                weight = float(parts[0])
                term = parts[1]

                item = {"term": term, "weight": weight}
                topic_terms.append(item)

            topic_dict["terms"] = topic_terms
            topics.append(topic_dict)

        if visualize_topics:
            vis = pyLDAvis.gensim_models.prepare(self.model, bow_corpus, self.dictionary)
            pyLDAvis.save_html(vis, 'lda2.html')

        return topics

    def predict(self, doc):
        bow_vector = self.dictionary.doc2bow(preprocess(doc))
        for index, score in sorted(self.model[bow_vector], key=lambda tup: -1 * tup[1]):
            print("Score: {}\t Topic: {}".format(score, self.model.print_topic(index, 5)))

        exit("Has to fill the LDA")
        return None
