from model_trainer_module.prediction.topics.text_topics import extract_hashtags, list_occurences, get_bigrams
from model_trainer_module.preprocess.preprocess import preprocess
from model_trainer_module.prediction.topics.LDA import LDA


class Topics:
    lda = None

    def __init__(self):

        print("|---- Initialize Topics classifier")
        self.lda = LDA()
        print("|---- Initialize sentiment classifier ended")

    def topics_of(self, text: list):
        """

        :param text: List of strings
        :return:
        """

        return self.tag_cloud(text), self.wordcloud(text), self.lda.lda_topics(text), self.bigrams(text)

    def tag_cloud(self, text: list):
        tag_cloud = []
        for t in text:
            tag_cloud.append(extract_hashtags(t))

        # remove empty lists
        tag_cloud = [ele for ele in tag_cloud if ele != []]
        all_tags_list = []
        for tag in tag_cloud:
            for t in tag:
                all_tags_list.append(t)
        all_tags_list = list_occurences(all_tags_list)

        return all_tags_list if all_tags_list else None

    def wordcloud(self, text):
        # Clean up the text
        total_text = ""
        for t in text:
            t = preprocess(t)
            if t:
                total_text = total_text + " ".join(t)


        return total_text

    def bigrams(self, text: list):
        total_text = ""
        for t in text:
            t = preprocess(t)
            if t:
                total_text = total_text + " ".join(t)
        total_bigrams = get_bigrams(total_text)

        one_str_bigram = ""
        for bigram in total_bigrams:
            one_str_bigram = one_str_bigram + " " + bigram[0] + "_" + bigram[1]

        return one_str_bigram
