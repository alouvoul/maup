import datetime
import json
import tweepy
import os
from model_trainer_module.utils.path_utils import resources_path

READ_CREDENTIALS_FROM_ENV_VARIABLES = True


class TwitterAPI:
    """
    Class that connects to a Twitter developer accoynt and fetches data via the API
    """
    api = None
    auth = None

    def __init__(self):
        credentials = self.read_credentials()
        self.auth = tweepy.OAuthHandler(credentials["api_key"], credentials["api_secret"])
        self.auth.set_access_token(credentials["access_token"], credentials["access_token_secret"])
        self.api = tweepy.API(auth_handler=self.auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    def read_credentials(self, filename="/credentials.json"):
        """
        Method which is used to read credentials for twitter development account. Used to init the connection in the
        __init__ method of the same class.
        :return: A dictionary with credentials
        """
        file = ""
        basedir = os.path.abspath(os.path.dirname(__file__))

        if not READ_CREDENTIALS_FROM_ENV_VARIABLES:
            try:
                with open(basedir + filename, "r") as c:
                    file = json.load(c)
            except IOError:
                print("Can't reach credentials file..." + basedir + filename)
                exit(1)
        else:
            file = {
                "api_key": os.environ.get("api_key"),
                "api_secret": os.environ.get("api_secret"),
                "access_token": os.environ.get("access_token"),
                "access_token_secret": os.environ.get("access_token_secret"),
            }

        credentials = {
            "api_key": file["api_key"],
            "api_secret": file["api_secret"],
            "access_token": file["access_token"],
            "access_token_secret": file["access_token_secret"],
        }
        return credentials

    def get_user(self, uid):
        try:
            user = self.api.get_user(uid)
        except:
            return None

        return user

    def get_user_tweets(self, screen_name, tweet_count=200):
        total_text = str("")

        try:
            posts = self.api.user_timeline(screen_name=screen_name, tweet_mode="extended", count=tweet_count,
                                           include_rts=1, exclude_replies=True)
            if not posts:
                return None
        except:
            return None
        # posts = posts._json
        # for post in posts:
        #     post = post._json
        #     if (not post['retweeted']) and ('RT @' not in post['full_text']):
        #         total_text = total_text + " " + post['full_text']

        return posts

    def get_user_tweets_list(self, screen_name, tweet_count=200):
        tweets = self.get_user_tweets(screen_name, tweet_count)
        tweet_list = []
        for tweet in tweets:
            tweet_list.append(tweet.full_text)
        return tweet_list



def get_tweets(screen_name):
    tweets = tw.get_user_tweets(screen_name, tweet_count=200)
    total_text = []
    tweets = tweets._json
    for tweet in tweets:
        tweet = tweet._json
        if (not tweet['retweeted']) and ('RT @' not in tweet['full_text']):
            total_text.append(tweet['full_text'])


# BEHAVIORAL FEATURES
#           'description_length', 'favorite_count', 'favourites_count', 'followers_count', 'friends_count',
#          'listed_count', 'no_of_hashtags', 'no_of_links', 'no_of_user_mentions', 'retweet_count', 'screename_length',
#          'statuses_count', 'tweet_length', 'upper'
if __name__ == "__main__":
    import pandas as pd

    tw = TwitterAPI()
    features = [
        'twitter_uid', 'screen_name', 'description', 'favourites_count', 'followers_count', 'friends_count',
        'statuses_count', 'listed_count',
    ]
    model_filename = resources_path("dataset/personality/new_personality_combined.csv")
    data = pd.read_csv(model_filename)
    df = []

    for id in data['twitter_uid']:
        user = tw.get_user(int(id))
        if user is None:
            continue

        user = user._json
        temp = [
            id,
            user['screen_name'],
            user['description'],
            user['favourites_count'],
            user['followers_count'],
            user['friends_count'],
            user['statuses_count'],
            user['listed_count'],
        ]
        df.append(temp)

    df = pd.DataFrame(df, columns=features)
    personality = pd.read_csv(resources_path("dataset/personality/personality.txt"), delimiter="\t")
    personality_behavioral = df.merge(personality, on='twitter_uid', how='left')
    uid_tweets_number = personality_behavioral.merge(data, on='twitter_uid', how='left')
    uid_tweets_number.to_csv(resources_path("dataset/personality/final_personality.csv"))



# data = data.drop(
#     ['Friends_count', 'Followers_count', 'Avg_Tweet', 'Avg_Word_length', 'Avg_Sentence_length', 'Avg_Punctuation',
#      'Avg_Capitalized_Words', 'Avg_Slang_words', 'Avg_emojis', 'Avg_words_found', 'tweets', 'retweets', 'images',
#      'urls'], axis=1)
# features = [
#     ['years_active', 'followers_count', 'friends_count', 'statuses_count', 'favourites_count',
#      'verified', 'average_likes', 'description']]
