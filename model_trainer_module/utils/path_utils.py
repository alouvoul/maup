import os
import re
from os import listdir
from os.path import join, isfile

resources_base = None


def resources_base_path():
    global resources_base

    if resources_base is not None:
        return resources_base

    cwd, filename = os.path.split(os.path.abspath(__file__))
    dirs = [directory for directory in listdir(cwd) if not isfile(join(cwd, directory))]

    while "resources" not in dirs:
        head, tail = os.path.split(cwd)

        cwd = head
        dirs = [directory for directory in listdir(cwd) if not isfile(join(cwd, directory))]

    resources_base = join(cwd, "resources")
    return resources_base


def resources_path(subpath: str):

    resources = resources_base_path()

    path = resources

    uses_slash = re.search(r"/", subpath) is not None
    uses_backslash = re.search(r"\\", subpath) is not None

    if uses_slash and not uses_backslash:
        parts = subpath.split("/")

        for part in parts:
            path = join(path, part)

        return path

    elif not uses_slash and uses_backslash:
        parts = subpath.split("/")

        for part in parts:
            path = join(path, part)

        return path
    else:
        return join(resources, subpath)
