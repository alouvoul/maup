import numpy as np
from datetime import datetime
from lime.lime_tabular import LimeTabularExplainer
from lime.lime_text import LimeTextExplainer


class Explainator:
    explainer = None

    def __init__(self, x_train, y_train, class_names, feature_names=None):
        """

        :param x_train:
        :param y_train:
        :param feature_names:
        """
        if feature_names is None:
            feature_names = x_train.columns

        self.explainer = LimeTabularExplainer(
            training_data=np.array(x_train),
            mode="classification",
            feature_names=feature_names,
            class_names=class_names,
            training_labels=np.array(y_train),
            discretize_continuous=False
        )

    def lime(self, model, instance=None, html_file=False, num_features=10):
        """
        :param model:
        :param instance:
        :param html_file:
        :param num_features:
        :return:
        """

        if instance is None:
            return None

        exp = self.explainer.explain_instance(instance[0], model.predict_proba, num_features=num_features)
        print("Lime explanation: ")
        exp.as_pyplot_figure(label=1).show()
        if html_file:
            exp.save_to_file(str(instance) + "_" + str(datetime.now()) + "_explain.html")

    def lime_text(self, model, text_sample, class_names):
        explainer = LimeTextExplainer(class_names=class_names)
        explanation = explainer.explain_instance(text_sample, model.predict_proba, num_features=10)
        explanation.as_pyplot_figure(label=1)

