import re

import emoji
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from nltk.corpus import stopwords

from model_trainer_module.feature_extraction.utils.EmojiEmotionFeature import EmojiEmotionFeature
from model_trainer_module.feature_extraction.utils.SentimentFeature import UnsupervisedSentiment
# from process.tokenizing import clear_emoji, demoji
from model_trainer_module.preprocess.preprocess import preprocess
from model_trainer_module.preprocess.Preprocessor import Preprocessor


class FeatureExtractor:

    def __init__(self):
        self.preprocessor = Preprocessor()
        self.words_analyzer = SentimentIntensityAnalyzer()
        self.unsupervised_sentiment = UnsupervisedSentiment()
        self.sentiments = {
            "Negative": 0,
            "Neutral": 1,
            "Positive": 2,
        }

    def number_of_mentions(self, doc):
        """
        Method which summarize the total number of mentions exist in the text. Mentions start with @ symbol.
        :param doc: Tokenized text
        :return: Total number of mentions
        """
        mentions = 0
        for token in doc:
            if token == "MENTION" or re.match(r"(@[A-Za-z0-9_]+)", token):
                mentions += 1
        return mentions / len(doc)

    def number_of_hashtags(self, doc):
        """
        Method which summarize the total number of hash tags exist in the text. Mentions start with # symbol.
        :param doc: Tokenized text
        :return: Total number of hash tags
        """
        hashtags = 0
        for token in doc:
            if token == "HASHTAG" or re.match(r"(#[A-Za-z0-9_]+)", token):
                hashtags += 1
        return hashtags / len(doc)

    def number_of_urls(self, doc):
        urls = 0
        for token in doc:
            if token == "URL" or re.match(r"(\b(http(s)?)(:)([\w\-.&=?\\\/]+)\b)", token):
                urls += 1
        return urls / len(doc)

    def character_flooding(self, doc):
        """
        Calculated the number of times that a doc contains token that has character flooding. For example, words like
        goooood and niceeee both have character flooding
        :param doc: Tokenized text
        :return: number of words which contains character flooding
        """
        flooding = 0
        for token in doc:
            if re.match(r'[A-Za-z]+([a-zA-Z])\1+', token):
                flooding += 1
        return flooding / len(doc)

    def number_of_punctuations(self, doc):
        """
        Total number of 3 punctuations are calculated. '?', '!' and '.' supposed that are meaningful as a feature.
        :param doc:Tokenized text of tokens
        :return: number of punctuations that are in the document
        """
        number_of_punctuations = 0
        for token in doc:
            number_of_punctuations += sum([1 for x in token if x in ['?', '!', '.']])
        return 0 if len(doc) == 0 else number_of_punctuations / len(doc)

    def capitals(self, doc):
        """
        Calculates the number of capital words in a document.
        :param doc: Array of string tokens
        :return: Total number of capital letter in a document
        """
        upper = 0
        for tokens in doc:
            if tokens.isupper():
                upper += 1
        return upper / len(doc)

    def vowel(self, doc):
        """
        Count the total number of vowels in the given array of token strings.
        :param doc: Array of string tokens
        :return: Number of vowels in the doc
        """
        number_of_words_without_vowels = []
        for token in doc:
            number_of_words_without_vowels.append(re.findall(r'\b[^AEIOU_0-9\W]+\b', token, flags=re.I))
        return len(number_of_words_without_vowels) / len(doc)

    def effect_of_words(self, doc):
        """
        This method used to count the sentiment of a document. Negative and positive scores added to the variable in
        order to get the average value of them.
        :param doc: Array of string tokens
        :return: Average values for positive and negative words, If the doc is empty return 0, 0
        """
        positive = 0
        negative = 0
        for token in doc:
            results = self.words_analyzer.polarity_scores(token)
            positive += results['pos']
            negative += results['neg']

        if len(doc) < 1:
            return 0, 0

        return positive / len(doc), negative / len(doc)

    def sentiment_vader(self, doc):
        """
        Used to calculated the sentiment of a document.
        :param doc: Array of string tokens
        :return: Return 0 for negative, 1 for neutral and 2 for positive sentiment in the doc
        """
        polarity = self.unsupervised_sentiment.vader_sentiment(doc)
        sentiment = self.unsupervised_sentiment.polarity(polarity["compound"], bound=0.05)
        return self.sentiments[sentiment]

    def sentiment_textblob(self, doc):
        """
        Used to calculated the sentiment of a document.
        :param doc: Array of string tokens
        :return: Return 0 for negative, 1 for neutral and 2 for positive sentiment in the doc
        """
        polarity, subjectivity = self.unsupervised_sentiment.text_blob_sentiment(doc)
        sentiment = self.unsupervised_sentiment.polarity(polarity)
        return self.sentiments[sentiment]

    def number_of_emoji(self, doc):
        """
        Method that finds the total number of emojies of a given array of tokens. Uses a simple regex to find the emoji.
        :param doc: Array of string tokens
        :return: Total number of emojies
        """
        emojies = self.preprocessor.clear_emoji(doc)
        total = 0
        for e in emojies:
            if re.search(r":[A-Za-z\-_]+:", e):
                total += 1
        return 0 if len(doc) == 0 else total / len(doc)

    def emojies(self, doc):
        """
        This method used to detect emoji. All the emoji are separating in 4 categories anger, fear, joy, sadness and
        all of the detected emoji add a value to their category.
        :param doc: Array of string tokens
        :return: anger, fear, joy, sadness average value
        """
        emoji_emo = EmojiEmotionFeature()
        emojies = self.__demoji(doc)
        emoji_emotion_values = []
        for e in emojies:
            if emoji_emo.is_emoji_name_like(e):

                if emoji_emo.exists_emoji_name(e):
                    emotions = emoji_emo.emotions_of_emoji_named(e)
                    emoji_emotion_values.append(emotions)

        if len(emoji_emotion_values) > 0:
            anger_total = 0
            fear_total = 0
            joy_total = 0
            sadness_total = 0

            for emotions in emoji_emotion_values:
                anger_total += emotions[0]
                fear_total += emotions[1]
                joy_total += emotions[2]
                sadness_total += emotions[3]

            anger = anger_total / len(emoji_emotion_values)
            fear = fear_total / len(emoji_emotion_values)
            joy = joy_total / len(emoji_emotion_values)
            sadness = sadness_total / len(emoji_emotion_values)

            return anger, fear, joy, sadness
        return 0, 0, 0, 0

    def __demoji(self, tokens):
        """
        This function describes each emoji with a text that can be later used for vectorization and ML predictions
        :param tokens:
        :return:
        """
        emoji_description = []
        for token in tokens:
            detect = emoji.demojize(token)
            emoji_description.append(detect)
        return emoji_description

    def words_avg(self, tokens: list):
        """

        :param tokens:
        :return:
        """
        total_length = 0
        for token in tokens:
            total_length += len(token)
        return total_length / len(tokens)

    def size_of_post(self, tokens: list):
        """

        :param tokens:
        :return:
        """
        return len(tokens)

    def number_of_stopwords(self, tokens: list):
        """
        Total number of stopwords in a tokens list.
        :param tokens: A list of tokens.
        :return: Integer number which is the total number of stopwords.
        """
        number_of_stopwords = 0
        stopwords_english = set(stopwords.words('english'))
        number_of_stopwords = sum(number_of_stopwords+1 if token in stopwords_english else number_of_stopwords for token in tokens)
        return number_of_stopwords

    def negation(self, tokens: list):
        negations = ["not", "no"]
        negative = 0
        tokens = preprocess(" ".join(tokens))
        for token in tokens:
            if token in negations:
                negative = 1

        return negative

    def period_counts(self, tokens: list):
        """

        :param tokens:
        :return: Number of periods in text and multi periods
        """
        period_counts = 0
        multi_period_count = 0

        for token in tokens:
            if re.search(r"\.", token) is not None:
                period_counts += 1

            if re.search(r"\.{2,}", token) is not None:
                multi_period_count += 1

        return period_counts, multi_period_count

    def exclamation_counts(self, tokens: list):
        """

        :param tokens:
        :return: Number of exclamation in text and multi exclamation
        """
        exclamation_counts = 0
        multi_exclamation_count = 0

        for token in tokens:
            if re.search(r"!", token) is not None:
                exclamation_counts += 1

            if re.search(r"!{2,}", token) is not None:
                multi_exclamation_count += 1

        return exclamation_counts, multi_exclamation_count

    def is_retweet(self, tokens: list):
        return 1 if 'RT' in tokens else 0

    def intensifiers(self):
        pass
