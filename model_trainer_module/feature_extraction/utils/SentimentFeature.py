from textblob import TextBlob
from vaderSentiment import vaderSentiment


class UnsupervisedSentiment:

    def text_blob_sentiment(self, data):
        """
        Method that predicts sentiment of a sentence using the TextBlob library.

        :param data: String sentence to predict the sentiment
        :return: Two values of polarity and subjectivity respectively
        """
        sentiment = TextBlob(' '.join(map(str, data))).sentiment
        return sentiment.polarity, sentiment.subjectivity

    def vader_sentiment(self, data):
        """
        Method that predicts sentiment of a sentence using the VADER library. The values are [-1.0, 1.0] which
        less than zero are negative sentiment and more than zero are positive sentiment.

        :param data: String sentence to predict the sentiment
        :return: Polarity score of the sentence which is [-1.0, 1.0].
        """
        vader = vaderSentiment.SentimentIntensityAnalyzer()
        return vader.polarity_scores(data)

    def polarity(self, polarity, bound=0.0):
        """
        Calculates the result of the polarity with the bounds given. For example for VADER values -0.5, 0.03 and 0.2
        are negative, neutral, and positive respectively.
        :param polarity:
        :param bound:
        :return:
        """
        if polarity == 0 or (-bound < polarity < bound):
            status = "Neutral"
        elif polarity >= bound:
            status = "Positive"
        else:
            status = "Negative"
        return status