import warnings
import gensim.downloader as api
import numpy as np


def query(token):
    return EmbeddingQuery(token)


class EmbeddingQuery:
    positive = []
    negative = []

    def __init__(self, token=None):
        self.positive = []
        self.negative = []

        if token is not None:
            self.positive.append(token)

    def plus(self, token: str):
        self.positive.append(token)
        return self

    def minus(self, token: str):
        self.negative.append(token)
        return self


FASTTEXT_300D = "fasttext-wiki-news-subwords-300"
GLOVE_TWITTER_100D = "glove-twitter-100"
GLOVE_TWITTER_200D = "glove-twitter-200"
GLOVE_WIKI_100D = "glove-wiki-gigaword-100"
GLOVE_WIKI_200D = "glove-wiki-gigaword-200"
GLOVE_WIKI_300D = "glove-wiki-gigaword-300"
W2V_NEWS_300D = "word2vec-google-news-300"


class EmbeddingModel:
    model = None
    dimension = 0
    uncased = False

    def __init__(self, model_type: str = GLOVE_WIKI_100D):
        self.model = api.load(model_type)
        self.dimension = self.model.vector_size
        self.uncased = "glove" in model_type

    def clean(self, token):
        if self.uncased:
            return token.lower().strip()
        else:
            return token.strip()

    def clean_tokens(self, tokens):
        return [self.clean(token) for token in tokens]

    def similarity_of(self, token_one: str, token_two: str):
        token_one = self.clean(token_one)
        token_two = self.clean(token_two)
        return self.model.similarity(w1=token_one, w2=token_two)

    def most_similar(self, token: str, count=10):
        token = self.clean(token)
        return self.model.most_similar(token, topn=count)

    def least_similar_to(self, token: str, count=10):
        token = self.clean(token)
        return self.model.most_similar(positive=[token, "opposite"], topn=count)

    def most_similar_to(self, tokens: list, count=10):
        tokens = self.clean_tokens(tokens)
        return self.model.most_similar(positive=tokens, topn=count)

    def least_similar_in(self, tokens):
        tokens = self.clean_tokens(tokens)
        return self.model.doesnt_match(tokens)

    def find(self, query: EmbeddingQuery, count=1):
        positive = self.clean_tokens(query.positive)
        negative = self.clean_tokens(query.negative)
        return self.model.most_similar(positive=positive, negative=negative, topn=count)

    def vector_of(self, token: str):
        token = self.clean(token)

        if token in self.model:
            return self.model[token]
        else:
            warnings.warn("Token '" + token + "' is out of vocabulary! The embedding model may have to be trained with "
                                              "the specific corpus.")
            return None

    def vectors_of(self, tokens: list):
        return np.array([self.vector_of(token) for token in tokens], dtype=np.float64)


def playground():
    model = EmbeddingModel()

    # Convert sentence to list of vectors
    sentence = ["A", "small", "text"]
    vectors_1 = model.vectors_of(sentence)

    # Convert a word to vetor
    word = "Nurse"
    vector_1 = model.vector_of(word)

    # Find closest words
    result_1 = model.find(query("woman").plus("doctor").minus("man"))

    # Find close words based on token
    results_1 = model.most_similar("basket", 10)

    # Find close words based on token
    results_2 = model.most_similar_to(["minister", "president"], 10)

    # Find the least close
    outlier = model.least_similar_in(["oranges", "melons", "apples", "boots"])

    # Similarity
    sim_1 = model.similarity_of("orange", "apple")
    sim_2 = model.similarity_of("sport", "flower")

    # Opposites
    o1 = model.least_similar_to("up")
    o2 = model.least_similar_to("hot")
    o3 = model.least_similar_to("good")

    return
