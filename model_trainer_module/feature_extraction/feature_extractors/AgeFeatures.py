import pandas as pd

from model_trainer_module.feature_extraction.FeatureExtractor import FeatureExtractor


class AgeFeatures:
    feature_extractor = None

    def __init__(self):
        self.feature_extractor = FeatureExtractor()

    # fit() doesn't do anything, this is a transformer class
    def fit(self, x, y=None):
        return self

    def transform(self, docs):
        """
        Transform the documents to feature vector. Used by Feature union to concat with vectorizers.
        :param docs: Array of docs
        :return: Dataframe of features
        """

        features = []
        for doc in docs:
            total_number_mentions = self.feature_extractor.number_of_mentions(doc)
            total_number_hashtags = self.feature_extractor.number_of_hashtags(doc)
            flooding = self.feature_extractor.character_flooding(doc)
            total_number_punctuations = self.feature_extractor.number_of_punctuations(doc)
            words_average_length = self.feature_extractor.words_avg(doc)
            capital_leters = self.feature_extractor.capitals(doc)
            avg_sentiment_pos, avg_sentiment_neg = self.feature_extractor.effect_of_words(doc)
            post_length = self.feature_extractor.size_of_post(doc)
            number_of_emojies = self.feature_extractor.number_of_emoji(doc)
            number_of_stopwords = self.feature_extractor.number_of_stopwords(doc)

            temp = [
                total_number_mentions,
                total_number_hashtags,
                flooding,
                total_number_punctuations,
                words_average_length,
                capital_leters,
                avg_sentiment_pos,
                avg_sentiment_neg,
                post_length,
                number_of_emojies,
                number_of_stopwords
            ]
            features.append(temp)
        return pd.DataFrame(features)

    def get_feature_names(self):
        return [
            'total_number_mentions',
            'total_number_hashtags',
            'flooding',
            'total_number_punctuations',
            'words_average_length',
            'capital_leters',
            'avg_sentiment_pos',
            'avg_sentiment_neg',
            'post_length',
            'number_of_emojies',
            'number_of_stopwords']
