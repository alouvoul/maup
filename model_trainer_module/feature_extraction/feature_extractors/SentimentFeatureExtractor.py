import pandas as pd
from model_trainer_module.feature_extraction.FeatureExtractor import FeatureExtractor


class SentimentFeatureExtractor:
    feature_extractor = None

    def __init__(self):
        self.feature_extractor = FeatureExtractor()

    def fit(self, x, y=None):
        return self

    def transform(self, docs):
        """
        Transform the documents to feature vector. Used by Feature union to concat with vectorizers.
        :param docs: Array of docs
        :return: Dataframe of features
        """
        print("|----    Feature extractor started")
        features = []
        for doc in docs:
            total_number_punctuations = self.feature_extractor.number_of_punctuations(doc)
            avg_sentiment_pos, avg_sentiment_neg = self.feature_extractor.effect_of_words(doc)
            number_of_emojies = self.feature_extractor.number_of_emoji(doc)
            anger, fear, joy, sadness = self.feature_extractor.emojies(doc)
            negation = self.feature_extractor.negation(doc)
            sentiment_vader = self.feature_extractor.sentiment_vader(doc)
            sentiment_textblob = self.feature_extractor.sentiment_textblob(doc)

            temp = [
                total_number_punctuations,
                avg_sentiment_pos,
                avg_sentiment_neg,
                number_of_emojies,
                anger,
                fear,
                joy,
                sadness,
                negation,
                sentiment_vader,
                sentiment_textblob
            ]
            features.append(temp)

        print("|----    Feature extractor finished...")
        return pd.DataFrame(features, columns=[
            "total_number_punctuations",
            "avg_sentiment_pos",
            "avg_sentiment_neg",
            "number_of_emojies",
            "anger",
            "fear",
            "joy",
            "sadness",
            "negation",
            "sentiment_vader",
            "sentiment_textblob",
        ])
