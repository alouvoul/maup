import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin

from model_trainer_module.feature_extraction.FeatureExtractor import FeatureExtractor


class PersonalityFeatureExtractor(BaseEstimator, TransformerMixin):
    feature_extractor = None

    def __init__(self):
        self.feature_extractor = FeatureExtractor()

    # fit() doesn't do anything, this is a transformer class
    def fit(self, x, y=None):
        return self

    def transform(self, docs):
        """
        Transform the documents to feature vector. Used by Feature union to concat with vectorizers.
        :param docs: Array of docs
        :return: Dataframe of features
        """

        features = []
        for doc in docs:
            doc = doc.split(" ")
            total_number_mentions = self.feature_extractor.number_of_mentions(doc)
            total_number_hashtags = self.feature_extractor.number_of_hashtags(doc)
            words_average_length = self.feature_extractor.words_avg(doc)
            capital_letters = self.feature_extractor.capitals(doc)
            post_length = self.feature_extractor.size_of_post(doc)
            is_retweet = self.feature_extractor.is_retweet(doc)
            flooding = self.feature_extractor.character_flooding(doc)
            emojies = self.feature_extractor.number_of_emoji(doc)
            period_counts, multi_period_count = self.feature_extractor.period_counts(doc)
            negation = self.feature_extractor.negation(doc)
            sentiment = self.feature_extractor.sentiment_vader(doc)
            no_of_links = self.feature_extractor.number_of_urls(doc)
            temp = [
                total_number_mentions,
                total_number_hashtags,
                words_average_length,
                capital_letters,
                post_length,
                is_retweet,
                flooding,
                emojies,
                period_counts,
                multi_period_count,
                negation,
                sentiment,
                no_of_links,
            ]
            features.append(temp)
        return pd.DataFrame(features, columns=self.get_feature_names())

    def get_feature_names(self):
        return [
            'total_number_mentions',
            'total_number_hashtags',
            'words_average_length',
            'capital_letters',
            'post_length',
            'is_retweet',
            'flooding',
            'emojies',
            'period_count',
            'multi_period_count',
            'negation',
            'sentiment',
            'no_of_links',
        ]

# BEHAVIORAL FEATURES
#           'description_length', 'favorite_count', 'favourites_count', 'followers_count', 'friends_count',
#          'listed_count', 'no_of_hashtags', 'no_of_links', 'no_of_user_mentions', 'retweet_count', 'screename_length',
#          'statuses_count', 'tweet_length', 'upper'
