import pandas as pd
from model_trainer_module.feature_extraction.FeatureExtractor import FeatureExtractor


class GenderFeatureExtractor:
    feature_extractor = None

    def __init__(self):
        self.feature_extractor = FeatureExtractor()

    def fit(self, x, y=None):
        return self

    def transform(self, docs):
        """
        Transform the documents to feature vector. Used by Feature union to concat with vectorizers.
        :param docs: Array of docs
        :return: Dataframe of features
        """
        print("|----    Feature extractor started")
        features = []
        for doc in docs['Tweets']:
            doc = doc.split(" ")
            number_of_mentions = self.feature_extractor.number_of_mentions(doc)
            number_of_hashtags = self.feature_extractor.number_of_hashtags(doc)
            number_of_urls = self.feature_extractor.number_of_urls(doc)
            flooding = self.feature_extractor.character_flooding(doc)
            total_number_punctuations = self.feature_extractor.number_of_punctuations(doc)
            words_average_length = self.feature_extractor.words_avg(doc)
            avg_sentiment_pos, avg_sentiment_neg = self.feature_extractor.effect_of_words(doc)
            post_length = self.feature_extractor.size_of_post(doc)
            number_of_emojies = self.feature_extractor.number_of_emoji(doc)
            number_of_stopwords = self.feature_extractor.number_of_stopwords(doc)

            temp = [
                number_of_mentions,
                number_of_hashtags,
                number_of_urls,
                flooding,
                total_number_punctuations,
                words_average_length,
                avg_sentiment_pos,
                avg_sentiment_neg,
                post_length,
                number_of_emojies,
                number_of_stopwords
            ]
            features.append(temp)

        tweet_features = pd.DataFrame(features, columns=[
            'number_of_mentions',
            'number_of_hashtags',
            'number_of_urls',
            "flooding",
            "total_number_punctuations",
            "words_average_length",
            "avg_sentiment_pos",
            "avg_sentiment_neg",
            "post_length",
            "number_of_emojies",
            "number_of_stopwords",

        ], index=docs.index)
        features = []
        for _, doc in docs.iterrows():
            description_length = self.feature_extractor.size_of_post(doc['description'])
            features.append(description_length)
        tweet_features['description_length'] = features
        tweet_features['years_active'] = docs['years_active']
        tweet_features['followers_count'] = docs['followers_count']
        tweet_features['friends_count'] = docs['friends_count']
        tweet_features['statuses_count'] = docs['statuses_count']
        tweet_features['favourites_count'] = docs['favourites_count']
        tweet_features['verified'] = docs['verified']
        tweet_features['average_likes'] = docs['average_likes']
        tweet_features['text_model'] = docs['text_model']
        print("|----    Feature extractor finished...")
        return tweet_features

    def get_feature_names(self):
        return [
            'number_of_mentions',
            'number_of_hashtags',
            'number_of_urls',
            "flooding",
            "total_number_punctuations",
            "words_average_length",
            "avg_sentiment_pos",
            "avg_sentiment_neg",
            "post_length",
            "number_of_emojies",
            "number_of_stopwords",
            'description_length',
            'years_active',
            'followers_count',
            'friends_count',
            'statuses_count',
            'favourites_count',
            'verified',
            'average_likes',
            'text_model'
        ]
