import pandas as pd

from model_trainer_module.feature_extraction.FeatureExtractor import FeatureExtractor


class GenderSecondModelFeatureExtractor:
    feature_extractor = None

    def __init__(self):
        self.feature_extractor = FeatureExtractor()

    def fit(self, x, y=None):
        return self

    def transform(self, docs):
        """
        Transform the documents to feature vector. Used by Feature union to concat with vectorizers.
        :param docs: Array of docs
        :return: Dataframe of features
        """

        print("|----    Second feature extractor started")
        features = []
        for _, doc in docs.iterrows():
            description_length = self.feature_extractor.size_of_post(doc['description'])
            features.append(description_length)

        # Add to the existing features the extracted from description
        docs['description_length'] = features
        print("|----    Second feature extractor completed")
        return docs.drop(['description', 'Tweets', 'ID'], axis=1)

    def get_feature_names(self):
        return [
            'description_length'
            ]
