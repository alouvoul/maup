import pandas as pd
from model_trainer_module.feature_extraction.FeatureExtractor import FeatureExtractor
from nltk.tokenize import word_tokenize
from model_trainer_module.prediction.emotion.UnsupervisedClassifier import UnsupervisedEmotionClassifier


class EmotionFeatureExtractor:
    feature_extractor = None

    def __init__(self):
        self.feature_extractor = FeatureExtractor()
        self.unsupervised_classifier = UnsupervisedEmotionClassifier()

    def fit(self, x, y=None):
        return self

    def transform(self, docs):
        """
        Transform the documents to feature vector. Used by Feature union to concat with vectorizers.
        :param docs: Array of docs
        :return: Dataframe of features
        """
        print("|----    Feature extractor started")
        features = []
        for doc in docs:
            doc = word_tokenize(doc)
            unsupervised_sentiment = self.unsupervised_classifier.emotions_of_words(doc)
            anger = unsupervised_sentiment.anger
            disgust = unsupervised_sentiment.disgust
            fear = unsupervised_sentiment.fear
            joy = unsupervised_sentiment.joy
            sadness = unsupervised_sentiment.sadness
            surprise = unsupervised_sentiment.surprise

            temp = [
                anger,
                disgust,
                fear,
                joy,
                sadness,
                surprise,
            ]
            features.append(temp)

        tweet_features = pd.DataFrame(features, columns=[
                'anger',
                'disgust',
                'fear',
                'joy',
                'sadness',
                'surprise',
        ], index=docs.index)

        print("|----    Feature emotion extractor finished...")
        return tweet_features

