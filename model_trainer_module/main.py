import nltk
# nltk.download('averaged_perceptron_tagger')
# nltk.download('wordnet')
# nltk.download('stopwords')
# nltk.download('names')

from datetime import datetime
from model_trainer_module.prediction.demographics.age.Age import Age

from model_trainer_module.prediction.demographics.gender.Gender import Gender
from model_trainer_module.prediction.emotion.Emotion import Emotion

from model_trainer_module.prediction.sentiment.Sentiment import Sentiment


def args_error():
    """
    Prints an argument error message and exits the application.
    :return: Nothing
    """
    print("Illegal argument error. Use --scrap SOURCE, --process or --server")
    exit(1)


if __name__ == "__main__":
    """
    The main entry point of the application.
    Based on the arguments the correct component is invoked or an argument error is thrown.
    """

    # arg_count = len(sys.argv)
    #
    # if arg_count < 2:
    #     args_error()

    print("Start Time = ", datetime.now().strftime("%H:%M:%S"))
    print('\n\n')

    # detect the application component to start
    aspect = 'server'
    # profile_builder = ProfileBuilder(aspect)
    # profile_builder.predict('boultonsteve')
    if aspect == 'age':
        print("|start_age_training")
        Age()
    elif aspect == 'gender':
        print("|start_gender_training")
        Gender()
    elif aspect == 'sentiment':
        print("start_sentiment_training()")
        Sentiment()
    elif aspect == 'emotion':
        print("start_emotion_training()")
        Emotion()
    elif aspect == 'personality':
        print("start_personality_training()")
    elif aspect == 'interests':
        print("start_interest_training()")
    elif aspect == 'Ethnicity':
        print("start_ethnicity_training()")
    elif aspect == 'education':
        print("start_education_training()")
    elif aspect == 'needs':
        print("start_needs_training()")
    else:
        from model_trainer_module.app.views import run
        port = 8080
        print("Starting Server at port " + str(port) + "...")
        run(debug=False, port=port)

    print("Shutting down...")

# EOF
