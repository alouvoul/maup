from model_trainer_module.preprocess.Preprocessor import Preprocessor
from model_trainer_module.preprocess.preprocess import preprocess


class ModelPreprocessor:

    def __init__(self):
        self.preprocessor = Preprocessor()

    def fit(self, x, y=None):
        return self

    def transform(self, df):
        print("|----        Text preprocess - clean up")

        # if isinstance(df, list):
        #     text = preprocess(df[0])
        #     return [" ".join(text)]

        df = df.map(lambda x: self.preprocessor.tokenize_text(x))
        df = df.map(lambda x: self.preprocessor.remove_all_stopwords(x))
        df = df.map(lambda x: self.preprocessor.remove_technical(x))
        df = df.map(lambda x: self.preprocessor.normalize_text(" ".join(x)))

        df = df.map(lambda x: self.preprocessor.tokenize_text(x))
        df = df.map(lambda x: self.preprocessor.remove_punctuations(x, discard_common_punctuations=True))
        df = df.map(lambda x: self.preprocessor.remove_all_stopwords(x))
        df = df.map(lambda x: self.preprocessor.lemmatize(x))
        df = df.map(lambda x: self.preprocessor.remove_empty(x))

        df = df.map(lambda x: self.preprocessor.split_tokens_from_letters(x))
        df = df.map(lambda x: self.preprocessor.remove_three_or_less_size_words(x))

        # df = df.map(lambda x: " ".join(x))

        print("|----        Text preprocess - clean up completed...")
        return df
