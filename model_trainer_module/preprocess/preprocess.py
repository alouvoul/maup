from model_trainer_module.preprocess.Preprocessor import Preprocessor

preprocessor = Preprocessor()


def preprocess(text, return_string=False):
    
    text = preprocessor.tokenize_text(text)
    text = preprocessor.remove_all_stopwords(text)
    text = preprocessor.remove_technical(text)
    text = preprocessor.normalize_text(" ".join(text))

    text = preprocessor.tokenize_text(text)
    text = preprocessor.remove_punctuations(text, discard_common_punctuations=True)
    text = preprocessor.remove_all_stopwords(text)
    text = preprocessor.lemmatize(text)
    text = preprocessor.remove_empty(text)

    text = preprocessor.split_tokens_from_letters(text)
    text = preprocessor.remove_three_or_less_size_words(text)

    if return_string:
        return " ".join(text)

    return text
