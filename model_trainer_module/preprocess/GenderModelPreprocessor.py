from model_trainer_module.preprocess.Preprocessor import Preprocessor
from model_trainer_module.preprocess.preprocess import preprocess
import re


class GenderModelPreprocessor:

    def __init__(self):
        self.preprocessor = Preprocessor()

    def fit(self, x, y=None):
        return self

    def remove_technical(self, tokens):
        """
        Replace technicalities (numbers, urls, mentions) from tokens
        :param tokens:
        :return:
        """
        technical = []

        for token in tokens:
            token = re.sub(r'\d+', '', token)  # remove numbers
            token = re.sub(r"(\b(http(s)?)(:)([\w\-.&=?\\\/]+)\b)", 'URL', token)
            token = re.sub("(@[A-Za-z0-9_]+)", "MENTION", token)  # Remove mentions
            technical.append(token)
        return technical

    def transform(self, df):
        print("|----        Text preprocess - clean up")

        df = df.map(lambda x: self.preprocessor.tokenize_text(x))
        df = df.map(lambda x: self.preprocessor.remove_all_stopwords(x))
        df = df.map(lambda x: self.remove_technical(x))
        df = df.map(lambda x: self.preprocessor.normalize_text(" ".join(x)))

        df = df.map(lambda x: self.preprocessor.tokenize_text(x))
        df = df.map(lambda x: self.preprocessor.remove_punctuations(x, discard_common_punctuations=True))
        df = df.map(lambda x: self.preprocessor.remove_all_stopwords(x))
        df = df.map(lambda x: self.preprocessor.lemmatize(x))
        df = df.map(lambda x: self.preprocessor.remove_empty(x))

        df = df.map(lambda x: self.preprocessor.split_tokens_from_letters(x))
        df = df.map(lambda x: self.preprocessor.remove_three_or_less_size_words(x))

        print("|----        Text preprocess - clean up completed...")
        return df