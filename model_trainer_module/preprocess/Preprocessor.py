
import re
import string

import emoji
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer

from model_trainer_module.preprocess.dictionaries import punctuation_normalizations, contraction_expansions


class Preprocessor:

    def __init__(self):
        self.lemmatizer = WordNetLemmatizer()

    def tokenize_text(self, text=""):
        """
        Function that splits text in separate tokens
        :param text:
        :param self: text to be tokenized
        :return:
        """
        if not self:
            return []
        return [token.lower() for token in text.split(" ")]

    def clear_emoji(self, tokens):
        """
        This function clear emojies from text and keeps them for further usage
        :param tokens: separated tokens
        :return:
        """
        split = []
        for token in tokens:
            splitted_emojies = emoji.get_emoji_regexp().split(token)
            split = split + splitted_emojies
        return split

    def demoji(self, tokens):
        """
        This function describes each emoji with a text that can be later used for vectorization and ML predictions
        :param tokens:
        :return:
        """
        emoji_description = []
        for token in tokens:
            detect = emoji.demojize(token)
            emoji_description.append(detect)
        return emoji_description

    def remove_all_stopwords(self, tokens):
        """
        Removes stopwords (common words such as "the","a" etc.) from tokens
        :param tokens:
        :return:
        """
        negations = ["not", "no"]
        stopwords_english = set(stopwords.words('english'))

        tokens_no_stopwords = []

        for token in tokens:

            if token not in stopwords_english:
                tokens_no_stopwords.append(token)
                continue

            replacement = ""

            for stopword in stopwords_english:

                if token != stopword:
                    continue

                if stopword in negations or re.search(r"[a-zA-Z]+n\'t", stopword) is not None:
                    replacement = "SEMANTIC_NEGATION"

                token = token.replace(stopword, replacement).strip()
                break

            if len(token) == 0:
                continue

            tokens_no_stopwords.append(token)

        return tokens_no_stopwords
        # tokens_no_stopwords = []
        # stopwords_english = set(stopwords.words('english'))
        # for token in tokens:
        #     if token not in stopwords_english:
        #         tokens_no_stopwords.append(token)
        # return tokens_no_stopwords

    def remove_stopwords(self, tokens):
        """
        Removes stopwords (common words such as "the","a" etc.) from tokens
        :param tokens:
        :return:
        """
        negations = ["not", "no"]
        stopwords_english = set(stopwords.words('english'))

        tokens_no_stopwords = []

        for token in tokens:

            if token not in stopwords_english:
                tokens_no_stopwords.append(token)
                continue

            replacement = ""

            for stopword in stopwords_english:

                if token != stopword:
                    continue

                if stopword in negations or re.search(r"[a-zA-Z]+n\'t", stopword) is not None:
                    replacement = "SEMANTIC_NEGATION"

                token = token.replace(stopword, replacement).strip()
                break

            if len(token) == 0:
                continue

            tokens_no_stopwords.append(token)

        return tokens_no_stopwords

    def lemmatize(self, tokens):
        """
        Function that performs lemmatization to tokens
        :param tokens:
        :return:
        """

        cleaned_data = []
        for token in tokens:
            lemma = self.lemmatizer.lemmatize(token)
            cleaned_data.append(lemma)
        return cleaned_data

    def normalize_text(self, text):
        """
        This function normalises text,in the sense that it removes spaces, tags, hashtags
        :param text:
        :return:
        """

        # normalization of double and single quotes from unicode to ascii characters
        for key in punctuation_normalizations:
            replacement = key
            for target in punctuation_normalizations[key]:
                text = text.replace(target, replacement)

        for key in contraction_expansions:
            text = text.replace(key, contraction_expansions[key])

        # fix missing space after punkt
        text = re.sub(r"(\b)?(\w+)([!?:;,.])(\w+)(\b|[!?:;,.])", " \\2\\3 \\4\\5 ", text, 0, re.MULTILINE)

        # fix space before punkt
        text = re.sub(r"(\s)([!?:;,.])(\s)?", "\\2\\3", text, 0, re.MULTILINE)

        # separate hashtags with spaces
        text = re.sub(r"((\s|\b|[\"\'><.,;:?!*+\-=$#{}\[\]\\/])(#[\w]+))", "\\2 \\3 ", text, 0, re.MULTILINE)

        # separate tags with spaces
        text = re.sub(r"((\s|\b|[\"\'><.,;:?!*+\-=$#{}\[\]\\/])(@[\w]+))", "\\2 \\3 ", text, 0, re.MULTILINE)

        return text

    def remove_technical(self, tokens):
        """
        Replace technicalities (numbers, urls, mentions) from tokens
        :param tokens:
        :return:
        """
        technical = []

        for token in tokens:
            token = re.sub(r'\d+', '', token)  # remove numbers
            token = re.sub(r"(\b(http(s)?)(:)([\w\-.&=?\\\/]+)\b)", '', token)
            token = re.sub("(@[A-Za-z0-9_]+)", "", token)  # Remove mentions
            technical.append(token)
        return technical

    def remove_empty(self, tokens):
        temp = []
        for token in tokens:
            if token:
                temp.append(token)
        return temp

    def remove_three_or_less_size_words(self, tokens):
        temp = []
        for token in tokens:
            if len(token) >= 3:
                temp.append(token)
        return temp

    def split_tokens_from_letters(self, tokens):
        temp = []
        for token in tokens:
            for t in token.split(" "):
                temp.append(t)
        return temp

    def remove_punctuations(self, tokens, discard_common_punctuations=False):
        """
        This function removes punctuation from a list of tokens
        :param discard_common_punctuations:
        :param tokens:
        :return:
        """
        cleaned_tokens = []

        punctuations = [x for x in string.punctuation]
        if not discard_common_punctuations:
            punctuations.remove('?')
            punctuations.remove('!')
            punctuations.remove('.')

        punctuations.append("'")
        punctuations.append('...')
        punctuations.append('#')
        punctuations.append('-')
        punctuations.append('$')
        punctuations.append('|')

        for token in tokens:
            # Sanitize string for punctuation chars
            for punkt in punctuations:
                replacement = ""

                if punkt == "'" and re.search(r"[a-zA-Z]'[a-zA-Z]", token) is not None:
                    replacement = " "
                elif punkt in ["(", "[", "{", ")", "]", ")"]:
                    replacement = " "

                token = token.replace(punkt, replacement)

            token = token.strip()

            if token and len(token) > 0:
                cleaned_tokens.append(token)
        return cleaned_tokens
